package id.ac.tazkia.spm.service;


import id.ac.tazkia.spm.dao.ColorDao;
import id.ac.tazkia.spm.dao.InstitutionDao;
import id.ac.tazkia.spm.dao.finance.FinanceDashboardDao;
import id.ac.tazkia.spm.dao.finance.FinanceTransaksiEnamBulanDao;
import id.ac.tazkia.spm.dao.smile.SmileJumlahDosenAktifDao;
import id.ac.tazkia.spm.dao.smile.SmileJumlahMahasiswaAktifDao;
import id.ac.tazkia.spm.dao.smile.SmilePerformanceMahasiswaDao;
import id.ac.tazkia.spm.dao.spmb.SpmbPendaftarPerProdiDao;
import id.ac.tazkia.spm.dao.spmb.SpmbPendaftarPerTahunDao;
import id.ac.tazkia.spm.dto.*;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.finance.FinanceDashboard;
import id.ac.tazkia.spm.entity.finance.FinanceTransaksiEnamBulan;
import id.ac.tazkia.spm.entity.smile.SmileJumlahDosenAktif;
import id.ac.tazkia.spm.entity.smile.SmileJumlahMahasiswaAktif;
import id.ac.tazkia.spm.entity.smile.SmilePerformanceMahasiswa;
import id.ac.tazkia.spm.entity.spmb.SpmbPendaftarPerProdi;
import id.ac.tazkia.spm.entity.spmb.SpmbPendaftarPerTahun;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Service
@EnableScheduling
public class ImporDataService {

    @Autowired
    private ColorDao colorDao;

    @Autowired
    private InstitutionDao institutionDao;

    @Autowired
    private SpmbPendaftarPerProdiDao spmbPendaftarPerProdiDao;

    @Autowired
    private SpmbPendaftarPerTahunDao spmbPendaftarPerTahunDao;

    @Autowired
    private SmilePerformanceMahasiswaDao smilePerformanceMahasiswaDao;

    @Autowired
    private FinanceDashboardDao financeDashboardDao;

    @Autowired
    private SmileJumlahDosenAktifDao smileJumlahDosenAktifDao;

    @Autowired
    private SmileJumlahMahasiswaAktifDao smileJumlahMahasiswaAktifDao;

    @Autowired
    private FinanceTransaksiEnamBulanDao financeTransaksiEnamBulanDao;

    @Autowired
    @Value("${finance.url}")
    private String keuanganUrl;

    @Autowired
    @Value("${hris.url}")
    private String hrisUrl;

    @Autowired
    @Value("${smile.url}")
    private String smileUrl;

    @Autowired
    @Value("${spmb.url}")
    private String spmbUrl;


    WebClient webClient1 = WebClient.builder()
            .baseUrl(keuanganUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    WebClient webClient2 = WebClient.builder()
            .baseUrl(hrisUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    WebClient webClient3 = WebClient.builder()
            .baseUrl(smileUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    WebClient webClient4 = WebClient.builder()
            .baseUrl(spmbUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();


    @PostConstruct
    public List<JumlahPendaftarProdiDto> getLaporanSpmb() {
        return webClient4.get()
                .uri(spmbUrl+"/api/pendaftarPerprodi")
                .retrieve().bodyToFlux(JumlahPendaftarProdiDto.class)
                .collectList()
                .block();
    }


    @PostConstruct
    public List<JumlahPendaftarPertahunDto> getJumlahPendaftarPertahun() {
        return webClient4.get()
                .uri(spmbUrl+"/api/pendaftarPertahun")
                .retrieve().bodyToFlux(JumlahPendaftarPertahunDto.class)
                .collectList()
                .block();
    }

    @PostConstruct
    public RateMahasiswaDto getPerformanceMahasiswa() {
        return webClient3.get()
                .uri(smileUrl+"/api/performance/mahasiswa")
                .retrieve().bodyToMono(RateMahasiswaDto.class)
                .block();
    }

    @PostConstruct
    public DashboardKeuanganDto getDashboardKeuangan() {
        return webClient1.get()
                .uri(keuanganUrl+"/api/dashboard/keuangan")
                .retrieve().bodyToMono(DashboardKeuanganDto.class)
                .block();
    }

    @PostConstruct
    public List<JumlahDosenAktifDto> getJumlahDosenAktif() {
        return webClient3.get()
                .uri(smileUrl+"/api/dosen/aktif")
                .retrieve().bodyToFlux(JumlahDosenAktifDto.class)
                .collectList()
                .block();
    }

    @PostConstruct
    public List<JumlahMahasiswaAktifDto> getJumlahMahasiswaAktif() {
        return webClient3.get()
                .uri(smileUrl+"/api/mahasiswa/aktif")
                .retrieve().bodyToFlux(JumlahMahasiswaAktifDto.class)
                .collectList()
                .block();
    }

    @PostConstruct
    public List<KeluarMasukTransaksiDto> getTransaksiEnamBulan() {
        return webClient1.get()
                .uri(keuanganUrl+"/api/transaksi/enambulan")
                .retrieve().bodyToFlux(KeluarMasukTransaksiDto.class)
                .collectList()
                .block();
    }


//    @Scheduled(cron = "59 44 10 * * *", zone = "Asia/Jakarta")
    @Scheduled(fixedDelay = 3600000)
    public void imporDataDariSpmb(){

        List<JumlahPendaftarProdiDto> jumlahPendaftar = getLaporanSpmb();
        if(jumlahPendaftar.isEmpty()){
        }else {
            Iterable<SpmbPendaftarPerProdi> spmbPendaftarPerProdis = spmbPendaftarPerProdiDao.findAll();
            if(spmbPendaftarPerProdis != null) {
                for (SpmbPendaftarPerProdi pendaftarPerProdi : spmbPendaftarPerProdis) {
                    spmbPendaftarPerProdiDao.delete(pendaftarPerProdi);
                }
            }
            for (JumlahPendaftarProdiDto api : jumlahPendaftar) {
                SpmbPendaftarPerProdi spmbPendaftarPerProdiBaru = new SpmbPendaftarPerProdi();
                spmbPendaftarPerProdiBaru.setNama(api.getNama());
                spmbPendaftarPerProdiBaru.setJumlah(api.getJumlah());
                spmbPendaftarPerProdiDao.save(spmbPendaftarPerProdiBaru);
            }
            System.out.println("Impor Pendaftar Per Prodi Sukses");
        }

        List<JumlahPendaftarPertahunDto> jumlahPendaftarPertahunDtos = getJumlahPendaftarPertahun();
        if(jumlahPendaftarPertahunDtos.isEmpty()){
        }else {
            List<SpmbPendaftarPerTahun> spmbPendaftarPerTahuns = spmbPendaftarPerTahunDao.findByStatusOrderByJumlahDesc(StatusRecord.AKTIF);
            if(spmbPendaftarPerTahuns != null) {
                for (SpmbPendaftarPerTahun pendaftarPerTahun : spmbPendaftarPerTahuns) {
                    spmbPendaftarPerTahunDao.delete(pendaftarPerTahun);
                }
            }
            Integer nomor = 1;
            for (JumlahPendaftarPertahunDto api : jumlahPendaftarPertahunDtos) {
                SpmbPendaftarPerTahun spmbPendaftarPerTahun = new SpmbPendaftarPerTahun();
                spmbPendaftarPerTahun.setNomor(nomor);
                spmbPendaftarPerTahun.setNama(api.getNama());
                spmbPendaftarPerTahun.setJumlah(api.getJumlah());
                spmbPendaftarPerTahunDao.save(spmbPendaftarPerTahun);
                nomor = nomor + 1;
            }
            System.out.println("Impor Pendaftar Per Tahun Sukses");
        }

        RateMahasiswaDto rateMahasiswaDto = getPerformanceMahasiswa();
        if(rateMahasiswaDto != null){
            SmilePerformanceMahasiswa smilePerformanceMahasiswa = smilePerformanceMahasiswaDao.findByStatus(StatusRecord.AKTIF);
            if(smilePerformanceMahasiswa != null){
                smilePerformanceMahasiswaDao.delete(smilePerformanceMahasiswa);
            }
            SmilePerformanceMahasiswa smilePerformanceMahasiswa1 = new SmilePerformanceMahasiswa();
            smilePerformanceMahasiswa1.setBulan(rateMahasiswaDto.getBulan());
            smilePerformanceMahasiswa1.setRate(rateMahasiswaDto.getRate());
            smilePerformanceMahasiswaDao.save(smilePerformanceMahasiswa1);
            System.out.println("Impor Performance Mahasiswa Sukses");
        }

        DashboardKeuanganDto dashboardKeuanganDto = getDashboardKeuangan();
        if(dashboardKeuanganDto != null){
            FinanceDashboard financeDashboard = financeDashboardDao.findByStatus(StatusRecord.AKTIF);
            if(financeDashboard != null){
                financeDashboardDao.delete(financeDashboard);
            }
            FinanceDashboard financeDashboard1 = new FinanceDashboard();
            financeDashboard1.setBudgetBalance(dashboardKeuanganDto.getBudgetBalance());
            financeDashboard1.setRequestFund(dashboardKeuanganDto.getRequestFund());
            financeDashboard1.setTotalBudget(dashboardKeuanganDto.getTotalBudget());
            financeDashboard1.setTotalPenerimaan(dashboardKeuanganDto.getTotalPenerimaan());
            financeDashboard1.setNamaPeriodeAnggaran(dashboardKeuanganDto.getNamaPeriodeAnggaran());
            financeDashboardDao.save(financeDashboard1);
            System.out.println("Impor Dashboar Keuangan Sukses");
        }


        List<JumlahDosenAktifDto> jumlahDosenAktifDtos = getJumlahDosenAktif();
        if(jumlahDosenAktifDtos != null){
            List<SmileJumlahDosenAktif> smileJumlahDosenAktifList = smileJumlahDosenAktifDao.findByStatusOrderByTotalDesc(StatusRecord.AKTIF);
            if(smileJumlahDosenAktifList != null){
                for(SmileJumlahDosenAktif sjd : smileJumlahDosenAktifList) {
                    smileJumlahDosenAktifDao.delete(sjd);
                }
            }

            for(JumlahDosenAktifDto jumlahDosenAktifDto : jumlahDosenAktifDtos){
                SmileJumlahDosenAktif smileJumlahDosenAktif = new SmileJumlahDosenAktif();
                smileJumlahDosenAktif.setHb(jumlahDosenAktifDto.getHb());
                smileJumlahDosenAktif.setLb(jumlahDosenAktifDto.getLb());
                smileJumlahDosenAktif.setTotal(jumlahDosenAktifDto.getTotal());
                smileJumlahDosenAktif.setKodeProdi(jumlahDosenAktifDto.getKodeProdi());
                smileJumlahDosenAktifDao.save(smileJumlahDosenAktif);
            }
            System.out.println("Impor Data dosen aktif sukses");
        }

        List<JumlahMahasiswaAktifDto> jumlahMahasiswaAktifDtos = getJumlahMahasiswaAktif();
        if(jumlahMahasiswaAktifDtos != null){
            List<SmileJumlahMahasiswaAktif> smileJumlahMahasiswaAktifs = smileJumlahMahasiswaAktifDao.findByStatusOrderByTotalDesc(StatusRecord.AKTIF);
            if(smileJumlahMahasiswaAktifs != null){
                for(SmileJumlahMahasiswaAktif sjd : smileJumlahMahasiswaAktifs) {
                    smileJumlahMahasiswaAktifDao.delete(sjd);
                }
            }

            for(JumlahMahasiswaAktifDto jumlahMahasiswaAktifDto : jumlahMahasiswaAktifDtos){
                SmileJumlahMahasiswaAktif smileJumlahMahasiswaAktif = new SmileJumlahMahasiswaAktif();
                smileJumlahMahasiswaAktif.setKodeProdi(jumlahMahasiswaAktifDto.getKodeProdi());
                smileJumlahMahasiswaAktif.setPria(jumlahMahasiswaAktifDto.getPria());
                smileJumlahMahasiswaAktif.setWanita(jumlahMahasiswaAktifDto.getWanita());
                smileJumlahMahasiswaAktif.setTotal(jumlahMahasiswaAktifDto.getTotal());
                smileJumlahMahasiswaAktifDao.save(smileJumlahMahasiswaAktif);
            }
            System.out.println("Impor Data Mahasiswa aktif sukses");
        }

        List<KeluarMasukTransaksiDto> keluarMasukTransaksiDtos = getTransaksiEnamBulan();
        if(keluarMasukTransaksiDtos != null){
            List<FinanceTransaksiEnamBulan> financeTransaksiEnamBulans = financeTransaksiEnamBulanDao.findByStatusOrderByAda(StatusRecord.AKTIF);
            if(financeTransaksiEnamBulans != null){
                for(FinanceTransaksiEnamBulan sjd : financeTransaksiEnamBulans) {
                    financeTransaksiEnamBulanDao.delete(sjd);
                }
            }

            Integer nomor = 1;
            for(KeluarMasukTransaksiDto keluarMasukTransaksiDto : keluarMasukTransaksiDtos){
                FinanceTransaksiEnamBulan financeTransaksiEnamBulan = new FinanceTransaksiEnamBulan();
                financeTransaksiEnamBulan.setAda(nomor);
                financeTransaksiEnamBulan.setNama(keluarMasukTransaksiDto.getNama());
                financeTransaksiEnamBulan.setPengeluaran(keluarMasukTransaksiDto.getPengeluaran());
                financeTransaksiEnamBulan.setPenerimaan(keluarMasukTransaksiDto.getPenerimaan());
                financeTransaksiEnamBulanDao.save(financeTransaksiEnamBulan);
                nomor = nomor + 1;
            }
            System.out.println("Impor Transaksi Enam Bulan sukses");
        }

    }



}
