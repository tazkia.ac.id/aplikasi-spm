package id.ac.tazkia.spm.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
@Entity
@Data
public class DosenStatus {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_dosen")
    private Dosen dosen;

    private String dosenStatus;

    private String nidn;

    private String fileNidn;

    private String jabatanAkademik;
    private String fileJabatanAkademik;
    private String praktisi;
    private String filePraktisi;
    private String perusahaan;
    private String filePerusahaan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi;

    private String tahun;



}
