package id.ac.tazkia.spm.entity;

import lombok.Data;

import jakarta.persistence.*;

@Data
@Entity
public class Color {

    @Id
    private Integer id;

    private String colorcol;

}
