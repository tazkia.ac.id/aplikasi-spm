package id.ac.tazkia.spm.entity.smile;

import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class SmilePerformanceMahasiswa {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;

    private String tahun;

    private String bulan;

    private BigDecimal rate;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
