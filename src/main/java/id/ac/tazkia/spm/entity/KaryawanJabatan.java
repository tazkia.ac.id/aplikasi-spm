package id.ac.tazkia.spm.entity;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class KaryawanJabatan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    @ManyToOne
    @JoinColumn(name = "id_jabatan")
    private Jabatan jabatan;

    private LocalDate startDate;
    private LocalDate endDate;

    private String keterangan;

    private String statusAktif;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;


}
