package id.ac.tazkia.spm.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
@Data
@Entity
public class Ewmp {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_dosen")
    private Dosen dosen;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi;

    private String tahun;

    private String psAkreditasi;
    private String psDalamPt;
    private String psLuarPt;
    private String penelitian;
    private String pkm;
    private String tugasTambahan;
    private String jumlahSks;
    private String rataSks;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
