package id.ac.tazkia.spm.entity.spmb;

import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
@Entity
@Data
public class SpmbPendaftarPerTahun {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;

    private Integer nomor;

    private String nama;

    private Integer jumlah;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
