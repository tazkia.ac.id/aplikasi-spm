package id.ac.tazkia.spm.entity;

import lombok.Data;
import org.apache.poi.hpsf.Decimal;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class Keuangan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String penggunaan;

    private BigDecimal unitTsDua;
    private BigDecimal unitTsSatu;
    private BigDecimal unitTs;
    private BigDecimal unitTsRata;

    private BigDecimal akreditasiTsDua;
    private BigDecimal akreditasiTsSatu;
    private BigDecimal akreditasiTs;
    private BigDecimal akreditasiTsRata;

    private BigDecimal persentaseTsDua;
    private BigDecimal persentaseTsSatu;
    private BigDecimal persentaseTsRata;

    private BigDecimal uppsTsSatu;
    private BigDecimal uppsTsDua;
    private BigDecimal uppsTsRata;

    @Enumerated(EnumType.STRING)
    private Jenis jenis;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalInput;

    private String userInput;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalDelete;

    private String userDelete;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalEdit;

    private String userEdit;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi;

    private String tahun;


}
