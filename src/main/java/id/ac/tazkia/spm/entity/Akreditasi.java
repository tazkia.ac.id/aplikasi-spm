package id.ac.tazkia.spm.entity;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Data
public class Akreditasi {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")

    private String id;
    @NotNull
    private String namaProgramStudi;
    private String strata;
    private String wilayah;
    private String noSk;
    private int tahunSk;
    private String peringkat;
    private Date kadaluarsa;
}