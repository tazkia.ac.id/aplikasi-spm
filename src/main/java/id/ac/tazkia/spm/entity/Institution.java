package id.ac.tazkia.spm.entity;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;


import jakarta.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Institution {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;
    @NotNull
    private String nama;
    private String alamat;
    private String telepon;
    private String keterangan;
    private String file;
    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalInput;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalUpdate;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalDelete;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalKedaluwarsa;

    private String noSk;

    private String tahunSk;

    private String fileSk;

    private String statusKedaluwarsa;

    private String statusAkreditasi;

}
