package id.ac.tazkia.spm.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
@Entity
@Data
public class KontribusiIntelektual {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_dosen")
    private Dosen dosen;

    private String matakuliahAkreditasi;

    private String matakuliahTidakAkreditasi;

    private String judulBahanAjar;

    private String tsDuaAkreditasi;
    private String tsSatuAkreditasi;
    private String tsAkreditasi;
    private String rataAkreditasi;

    private String tsDuaPt;
    private String tsSatuPt;
    private String tsPt;
    private String rataPt;
    private String rataJumlah;
    private String rekognisi;


    private String praktikNamaProduk;
    private String praktikDeskripsi;
    private String praktikKeterlibatan;
    private String praktikRekognisi;

    private String penelitianJudulArtikel;
    private String penelitianJumlahSitasi;
    private String urlGoogleScholar;
    private String penelitianRekognisi;

    private String kontribusiKegiatan;
    private String kontribusiOrganisasi;
    private String kontribusiRekognisi;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi;

    private String tahun;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;


}
