package id.ac.tazkia.spm.entity.finance;

import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class FinanceDashboard {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;

    private String namaPeriodeAnggaran;
    private BigDecimal totalBudget;
    private BigDecimal requestFund;
    private BigDecimal totalPenerimaan;
    private BigDecimal budgetBalance;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
