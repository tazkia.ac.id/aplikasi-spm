package id.ac.tazkia.spm.entity;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
@Entity
@Data
public class Keahlian {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_dosen")
    private Dosen dosen;

    private String ijazahMagister;
    private String fileIjazahMagister;

    private String ijazahDoctor;
    private String fileIjazahDoctor;

    private String bidangKeahlian;

    private String sertifikatPendidik;
    private String fileSertifikatPendidik;

    private String sertifikatKompetensi;
    private String fileSertifikatKompetensi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi;

    private String tahun;

}
