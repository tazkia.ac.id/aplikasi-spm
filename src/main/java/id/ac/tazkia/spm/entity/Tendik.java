package id.ac.tazkia.spm.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
@Entity
@Data
public class Tendik {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    private String statusTendik;

    private String diploma;
    private String diplomaFile;
    private String sarjana;
    private String sarjanaFile;

    private String magister;
    private String magisterFile;
    private String doktor;
    private String doktorFile;

    private String sertifikat;
    private String sertifikatFile;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi;

    private String tahun;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
