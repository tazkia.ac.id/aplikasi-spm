package id.ac.tazkia.spm.entity.finance;

import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class FinanceTransaksiEnamBulan {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;

    private Integer ada;
    private String nama;
    private BigDecimal penerimaan;
    private BigDecimal pengeluaran;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;


}
