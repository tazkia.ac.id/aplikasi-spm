package id.ac.tazkia.spm.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Data
@SQLDelete(sql = "UPDATE sarjana_terapan SET status = 'HAPUS', tanggal_delete=now() WHERE id=?")
public class SarjanaTerapan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String tahunMasuk;
    private String jumlahMahasiswaDiterima;

    private String akhirTsEnam;
    private String akhirTsLima;
    private String akhirTsEmpat;
    private String akhirTsTiga;
    private String akhirTsDua;
    private String akhirTsSatu;
    private String akhirTs;

    private String jumlahLulusan;
    private String rataMasaStudi;
    private String standarPendidikan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalInput;

    private String userInput;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalDelete;

    private String userDelete;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalEdit;

    private String userEdit;

    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private Prodi prodi;

    private String tahun;
}
