package id.ac.tazkia.spm.entity.smile;

import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

@Entity
@Data
public class SmileJumlahDosenAktif {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;

    private String kodeProdi;
    private Integer lb;
    private Integer hb;
    private Integer total;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
