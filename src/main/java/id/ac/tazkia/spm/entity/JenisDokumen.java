package id.ac.tazkia.spm.entity;

public enum JenisDokumen {
    LEGALITAS, STANDAR, KEBIJAKAN, STANDAR_SPMI, ISO
}
