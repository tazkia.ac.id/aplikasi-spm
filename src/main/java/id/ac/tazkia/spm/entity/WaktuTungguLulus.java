package id.ac.tazkia.spm.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Data
public class WaktuTungguLulus {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String tahunLulus;
    private String jumlahTerlacak;
    private String jumlahDipesan;
    private String wtSatu;
    private String wtDua;
    private String wtTiga;
    private String standar;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalInput;

    private String userInput;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalDelete;

    private String userDelete;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalEdit;

    private String userEdit;

    @ManyToOne
    @JoinColumn(name = "prodi")
    private Prodi prodi;

    private String tahun;

    @Enumerated(EnumType.STRING)
    private Jenis jenis;


}
