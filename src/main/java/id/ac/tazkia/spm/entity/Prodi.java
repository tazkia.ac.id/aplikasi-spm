package id.ac.tazkia.spm.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Prodi {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    private String namaProdi;

    private String keterangan;

    private String strata;

    private String tahunSk;

    private String statusProdi;

    private String noSk;

    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_fakultas")
    private Fakultas fakultas;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tglKadaluwarsa;

    private String userUpdate;

    private String userDelete;

    private LocalDate tglDelete;
    private LocalDate tglUpdate;
}
