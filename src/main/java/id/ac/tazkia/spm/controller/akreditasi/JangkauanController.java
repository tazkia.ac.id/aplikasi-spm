package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.entity.Prodi;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class JangkauanController {

    @GetMapping("/jangkauan")
    public String getJangkauan(Model model, @RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        return "akreditasi/jangkauan/list";
    }
}
