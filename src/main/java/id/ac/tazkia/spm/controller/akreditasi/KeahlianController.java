package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.DosenDao;
import id.ac.tazkia.spm.dao.KeahlianDao;
import id.ac.tazkia.spm.dao.ProdiDao;
import id.ac.tazkia.spm.entity.DosenStatus;
import id.ac.tazkia.spm.entity.Keahlian;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@Slf4j
public class KeahlianController {
    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private KeahlianDao keahlianDao;

    @Autowired
    private ProdiDao prodiDao;

    @Value("${upload.docIjazahS2}")
    private String docIjazahS2;

    @Value("${upload.docIjazahS3}")
    private String docIjazahS3;

    @Value("${upload.sertifikatPendidik}")
    private String sertifikatPendidik;

    @Value("${upload.sertifikatKompetensi}")
    private String sertifikatKompetensi;

    @GetMapping("/sdm/keahlian")
    public String keahlian(Model model, @RequestParam(required = false) String prodi, @RequestParam(required = false) String tahun,
                           @PageableDefault(size = 20) Pageable pageable){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        Optional<Prodi> optionalProdi = prodiDao.findById(prodi);
        model.addAttribute("namaProdi", optionalProdi.get());
        model.addAttribute("tahun", tahun);

        model.addAttribute("list", keahlianDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, optionalProdi.get(), tahun, pageable));
        return "akreditasi/sdm/keahlian/list";
    }

    @GetMapping("/sdm/keahlian/form")
    public String keahlianForm(Model model, @RequestParam(required = false) String prodi, @RequestParam(required = false) String tahun){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        Optional<Prodi> optionalProdi = prodiDao.findById(prodi);
        model.addAttribute("namaProdi", optionalProdi.get());
        model.addAttribute("tahun", tahun);
        model.addAttribute("dosen", dosenDao.findByStatus(StatusRecord.AKTIF));
        return "akreditasi/sdm/keahlian/form";
    }

    @PostMapping("/keahlian/post")
    public String postKeahlian(@Valid Keahlian keahlian, @RequestParam("doc1") MultipartFile file1,
                               @RequestParam("doc2") MultipartFile file2, @RequestParam("doc3")MultipartFile file3,
                               @RequestParam("doc4")MultipartFile file4, RedirectAttributes attributes) throws Exception{

        if (!file1.isEmpty() || file1 != null){
            String namaAsli = file1.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docIjazahS2 + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file1.transferTo(tujuan);

            keahlian.setFileIjazahMagister(idFile + "." + extension);
        }

        if (!file2.isEmpty() || file2 != null){
            String namaAsli = file2.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docIjazahS3 + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file2.transferTo(tujuan);

            keahlian.setFileIjazahDoctor(idFile + "." + extension);
        }

        if (!file3.isEmpty() || file3 != null){
            String namaAsli = file3.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = sertifikatPendidik + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file3.transferTo(tujuan);

            keahlian.setFileSertifikatPendidik(idFile + "." + extension);
        }

        if (!file4.isEmpty() || file4 != null){
            String namaAsli = file4.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = sertifikatKompetensi + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file4.transferTo(tujuan);

            keahlian.setFileSertifikatKompetensi(idFile + "." + extension);
        }
        Optional<Prodi> prodi = prodiDao.findById(keahlian.getProdi().getId());
        keahlian.setProdi(prodi.get());
        keahlian.setStatus(StatusRecord.AKTIF);
        keahlianDao.save(keahlian);
        attributes.addFlashAttribute("success", "Data berhasil ditambah");
        return "redirect:/sdm/keahlian?prodi=" + prodi.get().getId() + "&tahun=" + keahlian.getTahun();
    }



    @GetMapping("/view-doc/ijazah-magister/{doc}")
    public ResponseEntity<byte[]> viewDocIjazahMagister(@PathVariable Keahlian doc) throws Exception{
        String lokasiFile = docIjazahS2 + File.separator + doc.getFileIjazahMagister();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFileIjazahMagister().toLowerCase().endsWith("jpeg") || doc.getFileIjazahMagister().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFileIjazahMagister().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFileIjazahMagister().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/ijazah-doctor/{doc}")
    public ResponseEntity<byte[]> viewDocIjazahDoctor(@PathVariable Keahlian doc) throws Exception{
        String lokasiFile = docIjazahS3 + File.separator + doc.getFileIjazahDoctor();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFileIjazahDoctor().toLowerCase().endsWith("jpeg") || doc.getFileIjazahDoctor().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFileIjazahDoctor().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFileIjazahDoctor().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/sertifikat-pendidik/{doc}")
    public ResponseEntity<byte[]> viewDocSertifikatPendidik(@PathVariable Keahlian doc) throws Exception{
        String lokasiFile = sertifikatPendidik + File.separator + doc.getFileSertifikatPendidik();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFileSertifikatPendidik().toLowerCase().endsWith("jpeg") || doc.getFileSertifikatPendidik().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFileSertifikatPendidik().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFileSertifikatPendidik().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/sertifikat-keahlian/{doc}")
    public ResponseEntity<byte[]> viewDocSertifikatKeahlian(@PathVariable Keahlian doc) throws Exception{
        String lokasiFile = sertifikatPendidik + File.separator + doc.getFileSertifikatKompetensi();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFileSertifikatKompetensi().toLowerCase().endsWith("jpeg") || doc.getFileSertifikatKompetensi().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFileSertifikatKompetensi().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFileSertifikatKompetensi().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/keahlian/excel")
    public void downloadExcelStatusKepegawaian(HttpServletResponse response, @RequestParam Prodi prodi, @RequestParam String tahun)throws IOException {
        String[] column = {"No", "Nama Dosen", "Magister/ Magister Terapan/ Spesialis", "Doktor/ Doktor Terapan/ Spesialis", "Bidang Keahlian", "Sertifikat Pendidik Profesional" , "Sertifikat Kompetensi/ Profesi/Industri"};
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data Status Kepegawaian");

        List<Keahlian> keahlianList = keahlianDao.findByStatusAndTahunAndProdi(StatusRecord.AKTIF, tahun, prodi);
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < column.length; i++){
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(column[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        int baris = 1;

        for (Keahlian data : keahlianList){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(data.getDosen().getNama());
            row.createCell(2).setCellValue(data.getIjazahMagister());
            row.createCell(3).setCellValue(data.getIjazahDoctor());
            row.createCell(4).setCellValue(data.getBidangKeahlian());
            row.createCell(5).setCellValue(data.getSertifikatPendidik());
            row.createCell(6).setCellValue(data.getSertifikatKompetensi());
        }

        for (int i = 0; i < column.length; i++){
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=dataKeahlian_"+ LocalDate.now()+".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/keahlian/edit")
    public String keahlianEdit(Model model, @RequestParam Keahlian keahlian, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("keahlian", keahlian);
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);

        model.addAttribute("dosen", dosenDao.findByStatus(StatusRecord.AKTIF));
        return "akreditasi/sdm/keahlian/edit";
    }
}
