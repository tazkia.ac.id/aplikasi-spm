package id.ac.tazkia.spm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageRegulationController {

    @GetMapping("/regulations")
    public String pageRegulations(){

        return "page-regulations";
    }
}
