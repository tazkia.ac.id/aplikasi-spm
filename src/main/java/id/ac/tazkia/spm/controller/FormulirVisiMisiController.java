package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.FakultasDao;
import id.ac.tazkia.spm.dao.InstitutionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class FormulirVisiMisiController {
    @Autowired
    private InstitutionDao institutionDao;
    @Autowired
    private FakultasDao fakultasDao;
    @GetMapping("/formulir-visi-misi")
    public String FormVisiMisi(Model model){
        model.addAttribute("listFakultas",fakultasDao.findAll());
        return "formulir-visi-misi";
    }
}
