package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.config.RoleDao;
import id.ac.tazkia.spm.dao.config.UserDao;
import id.ac.tazkia.spm.entity.config.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.util.Optional;

@Controller @Slf4j
public class UserController {
    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/user")
    public String userGet(Model model, @PageableDefault(size = 10)Pageable pageable,
                          @RequestParam(required = false) String search){
        model.addAttribute("dropdown", "active");
        model.addAttribute("user", "active");

        if (StringUtils.hasText(search)){
            model.addAttribute("list", userDao.findByUsernameContainingIgnoreCase(search, pageable));
        }else {
            model.addAttribute("list", userDao.findByActive(true, pageable));
        }
        return "user/list";
    }

    @GetMapping("/user/add")
    public String userForm(Model model, @RequestParam(required = false)String id){
        model.addAttribute("dropdown", "active");
        model.addAttribute("user", "active");
        model.addAttribute("role", roleDao.findAll());
        if (id == null || id.isEmpty() || id.isBlank()){
            model.addAttribute("user", new User());
        }else {
            User getUser = userDao.findById(id).get();
            model.addAttribute("user", getUser);
        }
        return "user/form";
    }

    @PostMapping("/user/save")
    public String userSave(@Valid User user, RedirectAttributes attributes){
        try{
            user.setActive(true);
            user.setUser(user.getUsername());
            userDao.save(user);
            attributes.addFlashAttribute("success", "Berhasil disimpan");
        }catch (Exception e){
            log.error(e.getMessage());
            attributes.addFlashAttribute("error", "Terjadi Kesalahan : " + e.getMessage());
        }

        return "redirect:/user";
    }
}
