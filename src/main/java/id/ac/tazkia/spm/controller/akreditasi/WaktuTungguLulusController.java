package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.WaktuTungguLulusDao;
import id.ac.tazkia.spm.entity.Jenis;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.WaktuTungguLulus;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
public class WaktuTungguLulusController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private WaktuTungguLulusDao waktuTungguLulusDao;

    @GetMapping("/waktuTungguLulus")
    public String getWaktuTungguLulus(Model model, @RequestParam Prodi prodi, @RequestParam String tahun, @RequestParam Jenis jenis, @PageableDefault Pageable pageable){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        model.addAttribute("enum", jenis);
        if (jenis == Jenis.SARJANA){
            model.addAttribute("jenis", "sarjana");
        }else if (jenis == Jenis.DIPLOMA){
            model.addAttribute("jenis", "diploma");
        } else if (jenis == Jenis.SARJANA_TERAPAN) {
            model.addAttribute("jenis", "terapan");
        }

        model.addAttribute("list", waktuTungguLulusDao.findByStatusAndProdiAndTahunAndJenis(StatusRecord.AKTIF, prodi, tahun, jenis, pageable));

        return "akreditasi/waktuTungguLulus/list";
    }

    @PostMapping("/waktuTungguLulus/post")
    public String postWaktuTungguLulus(@Valid WaktuTungguLulus waktuTungguLulus, Authentication authentication, RedirectAttributes attributes){
        try{
            User user = currentUserService.currentUser(authentication);
            waktuTungguLulus.setUserInput(user.getUsername());
            waktuTungguLulus.setTanggalInput(LocalDate.now());
            waktuTungguLulusDao.save(waktuTungguLulus);
            attributes.addFlashAttribute("success", "Berhasil Disimpan");
            return "redirect:/waktuTungguLulus?prodi=" + waktuTungguLulus.getProdi().getId() + "&tahun=" + waktuTungguLulus.getTahun() + "&jenis=" + waktuTungguLulus.getJenis();
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Terjadi Kesalahan : " + e.getMessage());
            return "redirect:/waktuTungguLulus?prodi=" + waktuTungguLulus.getProdi().getId() + "&tahun=" + waktuTungguLulus.getTahun() + "&jenis=" + waktuTungguLulus.getJenis();
        }
    }
}
