package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.DosenDao;
import id.ac.tazkia.spm.dao.DosenStatusDao;
import id.ac.tazkia.spm.dao.KeahlianDao;
import id.ac.tazkia.spm.dao.ProdiDao;
import id.ac.tazkia.spm.entity.DosenStatus;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@Slf4j
public class StatusKepegawaianController {
    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private KeahlianDao keahlianDao;

    @Autowired
    private ProdiDao prodiDao;

    @Autowired
    private DosenStatusDao dosenStatusDao;

    @Value("${upload.docNidn}")
    private String uploadNidn;

    @Value("${upload.docJabatan}")
    private String docJabatan;

    @Value("${upload.docPraktisi}")
    private String docPraktisi;

    @Value("${upload.docPerusahaan}")
    private String docPerusahaan;

    @GetMapping("/sdm/statusKepegawaian")
    public String statusKepegawaian(Model model, @RequestParam(required = false) String prodi, @RequestParam(required = false) String tahun,
                                    @PageableDefault(size = 10) Pageable pageable){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        Optional<Prodi> optionalProdi = prodiDao.findById(prodi);
        model.addAttribute("namaProdi", optionalProdi.get());
        model.addAttribute("tahun", tahun);
        model.addAttribute("list", dosenStatusDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, optionalProdi.get(), tahun, pageable));
        return "akreditasi/sdm/StatusKepegawaian/list";
    }

    @GetMapping("/sdm/statusKepegawaian/form")
    public String statusKepegawaianForm(Model model, @RequestParam(required = false) String prodi, @RequestParam(required = false) String tahun){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        Optional<Prodi> optionalProdi = prodiDao.findById(prodi);
        model.addAttribute("namaProdi", optionalProdi.get());
        model.addAttribute("tahun", tahun);
        model.addAttribute("dosen", dosenDao.findByStatus(StatusRecord.AKTIF));
        return "akreditasi/sdm/StatusKepegawaian/form";
    }

    @PostMapping("/statusKepegawaian/post")
    public String postStatusKepegawaian(@Valid DosenStatus dosenStatus, @RequestParam("doc1") MultipartFile file1,
                                        @RequestParam("doc2") MultipartFile file2, @RequestParam("doc3")MultipartFile file3,
                                        @RequestParam("doc4")MultipartFile file4,
                                        RedirectAttributes attributes) throws Exception{

        if (!file1.isEmpty() || file1 != null){
            String namaAsli = file1.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadNidn + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file1.transferTo(tujuan);

            dosenStatus.setFileNidn(idFile + "." + extension);
        }

        if (!file2.isEmpty() || file2 != null){
            String namaAsli = file2.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docJabatan + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file2.transferTo(tujuan);

            dosenStatus.setFileJabatanAkademik(idFile + "." + extension);
        }

        if (!file3.isEmpty() || file3 != null){
            String namaAsli = file3.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docPraktisi + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file3.transferTo(tujuan);

            dosenStatus.setFilePraktisi(idFile + "." + extension);
        }

        if (!file4.isEmpty() || file4 != null){
            String namaAsli = file4.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docPerusahaan + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file4.transferTo(tujuan);

            dosenStatus.setFilePerusahaan(idFile + "." + extension);
        }

        Optional<Prodi> prodi = prodiDao.findById(dosenStatus.getProdi().getId());
        dosenStatus.setProdi(prodi.get());
        dosenStatus.setStatus(StatusRecord.AKTIF);

        dosenStatusDao.save(dosenStatus);

        attributes.addFlashAttribute("success", "Data berhasil ditambah");
        return "redirect:/sdm/statusKepegawaian?prodi=" + prodi.get().getId() + "&tahun=" + dosenStatus.getTahun();

    }

    @GetMapping("/statusKepegawaian/delete/{id}")
    public String deleteStatusKepegawaian(@PathVariable String id, RedirectAttributes attributes){
        Optional<DosenStatus> dosenStatus = dosenStatusDao.findById(id);
        dosenStatus.get().setStatus(StatusRecord.HAPUS);
        dosenStatusDao.save(dosenStatus.get());
        attributes.addFlashAttribute("delete", "detele");
        return "redirect:/sdm/statusKepegawaian?prodi=" + dosenStatus.get().getProdi().getId() + "&tahun=" + dosenStatus.get().getTahun();
    }

    @GetMapping("/view-doc/nidn/{doc}")
    public ResponseEntity<byte[]> viewDocNidn(@PathVariable DosenStatus doc) throws Exception{
        String lokasiFile = uploadNidn + File.separator + doc.getFileNidn();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFileNidn().toLowerCase().endsWith("jpeg") || doc.getFileNidn().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFileNidn().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFileNidn().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/jabatan/{doc}")
    public ResponseEntity<byte[]> viewDocJabatan(@PathVariable DosenStatus doc) throws Exception{
        String lokasiFile = docJabatan + File.separator + doc.getFileJabatanAkademik();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFileJabatanAkademik().toLowerCase().endsWith("jpeg") || doc.getFileJabatanAkademik().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFileJabatanAkademik().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFileJabatanAkademik().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/praktisi/{doc}")
    public ResponseEntity<byte[]> viewDocPraktisi(@PathVariable DosenStatus doc) throws Exception{
        String lokasiFile = docPraktisi + File.separator + doc.getFilePraktisi();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFilePraktisi().toLowerCase().endsWith("jpeg") || doc.getFilePraktisi().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFilePraktisi().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFilePraktisi().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/perusahaan/{doc}")
    public ResponseEntity<byte[]> viewDocPerusahaan(@PathVariable DosenStatus doc) throws Exception{
        String lokasiFile = docPerusahaan + File.separator + doc.getFilePerusahaan();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFilePerusahaan().toLowerCase().endsWith("jpeg") || doc.getFilePerusahaan().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFilePerusahaan().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFilePerusahaan().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/statusKepegawaian/excel")
    public void downloadExcelStatusKepegawaian(HttpServletResponse response, @RequestParam Prodi prodi, @RequestParam String tahun)throws IOException {
        String[] column = {"No", "Nama Dosen", "Status", "NIDN/NIDK", "Jabatan Akademik", "Akademisi/Praktisi" , "Perusahaan/Industri"};
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data Status Kepegawaian");

        List<DosenStatus> dosenStatusList = dosenStatusDao.findByStatusAndTahunAndProdi(StatusRecord.AKTIF, tahun, prodi);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < column.length; i++){
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(column[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        int baris = 1;

        for (DosenStatus data : dosenStatusList){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(data.getDosen().getNama());
            row.createCell(2).setCellValue(data.getDosenStatus());
            row.createCell(3).setCellValue(data.getNidn());
            row.createCell(4).setCellValue(data.getJabatanAkademik());
            row.createCell(5).setCellValue(data.getPraktisi());
            row.createCell(6).setCellValue(data.getPerusahaan());
        }

        for (int i = 0; i < column.length; i++){
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=dataStatusKepegawaian_"+ LocalDate.now()+".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/statusKepegawaian/edit")
    public String editStatusKepegawaian(Model model, @RequestParam String id, @RequestParam String prodi, @RequestParam String tahun){
        Optional<DosenStatus> dosenStatusOptional = dosenStatusDao.findById(id);
        model.addAttribute("statusKepegawaian", dosenStatusOptional);
        model.addAttribute("dosen", dosenDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        Optional<Prodi> optionalProdi = prodiDao.findById(prodi);
        model.addAttribute("namaProdi", optionalProdi.get());
        model.addAttribute("tahun", tahun);
        return "akreditasi/sdm/StatusKepegawaian/edit";
    }
}
