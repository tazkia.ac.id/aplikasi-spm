package id.ac.tazkia.spm.controller.api;

import id.ac.tazkia.spm.dao.KaryaIlmiahDao;
import id.ac.tazkia.spm.dto.BaseResponseApiDto;
import id.ac.tazkia.spm.entity.KaryaIlmiah;
import id.ac.tazkia.spm.entity.KepuasanPengguna;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Optional;

@RestController @Slf4j
@RequestMapping("/api")
public class ApiKaryaIlmiahController {
    @Autowired
    private KaryaIlmiahDao karyaIlmiahDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/get/karyaIlmiah")
    public ResponseEntity<BaseResponseApiDto> getKaryaIlmiah(@RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        try{

            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .data(karyaIlmiahDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF,prodi, tahun, pageable))
                            .build()
            );

        }catch (Exception e){
            log.error("[GET KARYA ILMIAH] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PostMapping("/post/karyaIlmiah")
    public ResponseEntity<BaseResponseApiDto> postKepuasanPengguna(@Valid KaryaIlmiah karyaIlmiah,
                                                                   Authentication authentication, BindingResult bindingResult){
        try{
            User user = currentUserService.currentUser(authentication);
            if (bindingResult.hasErrors()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new BaseResponseApiDto("ERR01", "Invalid Request Body, "
                                + bindingResult.getFieldError().getDefaultMessage()));
            }
            karyaIlmiah.setTanggalInput(LocalDate.now());
            karyaIlmiah.setUserInput(user.getUsername());
            karyaIlmiahDao.save(karyaIlmiah);
            return ResponseEntity.status(HttpStatus.OK).body(
                    BaseResponseApiDto.builder()
                            .responseCode("200")
                            .responseMessage("success")
                            .build());

        }catch (Exception e){
            log.error("[POST KARYA ILMIAH] - [ERROR] : Unable to post data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());

        }
    }

    @GetMapping("/delete/karyaIlmiah")
    public ResponseEntity<BaseResponseApiDto> deleteKepuasanPengguna(@RequestParam String id,
                                                                     Authentication authentication){
        try{
            User user = currentUserService.currentUser(authentication);
            KaryaIlmiah karyaIlmiah = karyaIlmiahDao.cariId(id);
            karyaIlmiah.setStatus(StatusRecord.HAPUS);
            karyaIlmiah.setTanggalDelete(LocalDate.now());
            karyaIlmiah.setUserInput(user.getUsername());
            karyaIlmiahDao.save(karyaIlmiah);
            return ResponseEntity.status(HttpStatus.OK).body(
                    BaseResponseApiDto.builder()
                            .responseCode("200")
                            .responseMessage("success")
                            .build());

        }catch (Exception e){
            log.error("[Delete KARYA ILMIAH] - [ERROR] : Unable to delete data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());

        }
    }

    @GetMapping("/get/edit")
    public ResponseEntity<BaseResponseApiDto> getIdKaryaIlmiah(@RequestParam String id){
        try {
            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .data(karyaIlmiahDao.getDataEdit(id))
                            .build()
            );
        }catch (Exception e){
            log.error("[GET KARYA ILMIAH] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PostMapping("/update/karyaIlmiah")
    public ResponseEntity<BaseResponseApiDto> updteKaryaIlmiah(@Valid KaryaIlmiah karyaIlmiah, Authentication authentication,
                                                               BindingResult bindingResult){
        try{
            User user = currentUserService.currentUser(authentication);
            if (bindingResult.hasErrors()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new BaseResponseApiDto("ERR01", "Invalid Request Body, "
                                + bindingResult.getFieldError().getDefaultMessage()));
            }
            Optional<KaryaIlmiah> karyaIlmiahOptional = karyaIlmiahDao.findById(karyaIlmiah.getId());
            KaryaIlmiah karyaIlmiahUpdate = karyaIlmiahOptional.get();

            karyaIlmiahUpdate.setMahasiswa(karyaIlmiah.getMahasiswa());
            karyaIlmiahUpdate.setJudul(karyaIlmiah.getJudul());
            karyaIlmiahUpdate.setJumlahSitasi(karyaIlmiah.getJumlahSitasi());
            karyaIlmiahUpdate.setStandar(karyaIlmiah.getStandar());
            karyaIlmiahUpdate.setProdi(karyaIlmiah.getProdi());
            karyaIlmiahUpdate.setTahun(karyaIlmiahUpdate.getTahun());

            karyaIlmiahUpdate.setTanggalEdit(LocalDate.now());
            karyaIlmiahUpdate.setUserEdit(user.getUsername());
            karyaIlmiahDao.save(karyaIlmiahUpdate);
            return ResponseEntity.status(HttpStatus.OK).body(
                    BaseResponseApiDto.builder()
                            .responseCode("200")
                            .responseMessage("success")
                            .build());

        }catch (Exception e){
            log.error("[UPDATE KARYA ILMIAH] - [ERROR] : Unable to post data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());

        }
    }
}
