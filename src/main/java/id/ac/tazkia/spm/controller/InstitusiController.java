package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.InstitutionDao;
import id.ac.tazkia.spm.entity.Dokumen;
import id.ac.tazkia.spm.entity.Institution;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.UUID;

@Controller @Slf4j
public class InstitusiController {
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private InstitutionDao institutionDao;

    @Value("${upload.docLegalitas}")
    private String uploadLegalitas;

    @GetMapping("/institusi")
    public String getInstitusi(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("institusi", "active");
        model.addAttribute("list", institutionDao.findByStatus(StatusRecord.AKTIF));
        return "institusi/list";
    }

    @GetMapping("/institusi/form")
    public String getFormInstitusi(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("institusi", "active");
        model.addAttribute("data", new Institution());
        return "institusi/form";
    }

    @GetMapping("/institusi/edit")
    public String getEditInstitusi(Model model,@RequestParam String id){
        model.addAttribute("dropdown", "active");
        model.addAttribute("institusi", "active");
        model.addAttribute("data", institutionDao.findById(id));
        return "institusi/form";
    }


    @PostMapping("/institusi/add")
    public String postInstitusi(@Valid Institution institution, @RequestParam("doc")MultipartFile file,
                                Authentication authentication, RedirectAttributes attributes)throws Exception{
        try{
            User user = currentUserService.currentUser(authentication);
            if (!file.isEmpty() || file != null){
                String namaAsli = file.getOriginalFilename();
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }
                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = uploadLegalitas + File.separator;
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                file.transferTo(tujuan);

                institution.setFile(idFile + "." + extension);
            }
            institution.setStatus(StatusRecord.AKTIF);
            institution.setUserInsert(user.getUsername());
            institution.setTanggalInput(LocalDate.now());
            institutionDao.save(institution);
            attributes.addFlashAttribute("success", "Data berhasil ditambah");
            return "redirect:/institusi";

        }catch (Exception e) {
            attributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/institusi";
        }
    }

    @GetMapping("/institusi/delete/{id}")
    public String deleteInstitusi(@PathVariable Institution id, Authentication authentication,
                                  RedirectAttributes attributes){
        try {
            User user = currentUserService.currentUser(authentication);
            id.setStatus(StatusRecord.HAPUS);
            id.setTanggalDelete(LocalDate.now());
            institutionDao.save(id);
            attributes.addFlashAttribute("delete", "detele");
            return "redirect:/institusi";
        }catch (Exception e){
            attributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/institusi";
        }

    }

    @GetMapping("/view-doc/ints/{doc}")
    public ResponseEntity<byte[]> perbaikan(@PathVariable Institution doc) throws Exception{
        String lokasiFile = uploadLegalitas + File.separator + doc.getFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFile().toLowerCase().endsWith("jpeg") || doc.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
