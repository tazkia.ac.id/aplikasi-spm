package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.DokumenDao;
import id.ac.tazkia.spm.dao.KaryawanDao;
import id.ac.tazkia.spm.entity.Dokumen;
import id.ac.tazkia.spm.entity.JenisDokumen;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Controller @Slf4j
public class IsoController {
    @Autowired
    private DokumenDao dokumenDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.docLegalitas}")
    private String uploadStandar;

    @GetMapping("/iso")
    public String getIso(Model model, @RequestParam(required = false) String search, @PageableDefault(size = 10)Pageable pageable){
        model.addAttribute("dropdown", "active");
        model.addAttribute("iso", "active");

        if (StringUtils.hasText(search)){
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisAndNamaDokumenContainingIgnoreCase
                            (StatusRecord.AKTIF, JenisDokumen.ISO, search, pageable));
        }else {
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisOrderByTahunDesc(StatusRecord.AKTIF, JenisDokumen.ISO, pageable));
        }

        return "iso/list";
    }

    @GetMapping("/iso/add")
    public String formStandarSpmi(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("iso", "active");
        model.addAttribute("karyawan", karyawanDao.findByStatus(StatusRecord.AKTIF));

        return "iso/form";
    }

    @PostMapping("/iso/save")
    public String saveIso(@Valid Dokumen iso, Authentication authentication,
                          @RequestParam("doc") MultipartFile file,
                          RedirectAttributes attributes) throws Exception{
        try {
            User user = currentUserService.currentUser(authentication);
            if (!file.isEmpty() || file != null){
                String namaAsli = file.getOriginalFilename();
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }
                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = uploadStandar + File.separator;
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                file.transferTo(tujuan);

                iso.setFile(idFile + "." + extension);
            }
            iso.setStatus(StatusRecord.AKTIF);
            iso.setUserInsert(user.getUsername());
            iso.setTanggalInput(LocalDate.now());
            iso.setJenis(JenisDokumen.ISO);
            dokumenDao.save(iso);

            attributes.addFlashAttribute("success", "Berhasil");
            return "redirect:/iso";
        }catch (Exception e){
            attributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/iso";
        }

    }

    @GetMapping("/iso/delete/{iso}")
    public String isoDelete(@PathVariable Dokumen iso, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        iso.setStatus(StatusRecord.HAPUS);
        iso.setUserDelete(user.getUsername());
        iso.setTanggalDelete(LocalDate.now());
        attributes.addFlashAttribute("delete", "detele");
        dokumenDao.save(iso);
        return "redirect:/iso";
    }

    @GetMapping("/iso/update/{iso}")
    public String getUpdate(Model model,@PathVariable Dokumen iso){
        model.addAttribute("iso", iso);
        return "iso/update";
    }

    @PostMapping("/iso/update")
    public String updateLegalitas(@Valid Dokumen dokumen, Authentication authentication, RedirectAttributes attributes)throws Exception{
        try {
            User user = currentUserService.currentUser(authentication);
            Optional<Dokumen> dokumenOptional = dokumenDao.findById(dokumen.getId());
            Dokumen dokumenUpdate = dokumenOptional.get();
            dokumen.setFile(dokumenOptional.get().getFile());
            dokumen.setStatus(StatusRecord.AKTIF);
            dokumen.setUserInsert(dokumenOptional.get().getUserInsert());
            BeanUtils.copyProperties(dokumen, dokumenUpdate);
            dokumenUpdate.setUserUpdate(user.getUsername());
            dokumenUpdate.setTanggalUpdate(LocalDate.now());
            dokumenDao.save(dokumenUpdate);
            attributes.addFlashAttribute("update", "Berhasil di update");
        }catch (Exception e){
            log.error("Error : " + e.getMessage());
            attributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/iso";
    }
}
