package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.AkreditasiDao;
import id.ac.tazkia.spm.entity.Akreditasi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.thymeleaf.model.IModel;

@Controller
public class DataAkreditasiController {
    @Autowired
    private AkreditasiDao akreditasiDao;

    @PostMapping("/simpanData")
    public String simpanData(@ModelAttribute Akreditasi akreditasi, Model model){
        akreditasiDao.save(akreditasi);
        model.addAttribute("successMessage", "Data berhasil disimpan");
        return "redirect:/setelah-login/data-akreditasi";
    }

    @GetMapping("/data-akreditasi")
    public String dataAkreditasi(Model model) {
        model.addAttribute("listAkreditasi", akreditasiDao.findAll());
        model.addAttribute("akreditasi", new Akreditasi() );
        return "setelah-login/data-akreditasi";
    }
}