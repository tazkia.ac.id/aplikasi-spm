package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.DokumenDao;
import id.ac.tazkia.spm.dao.DokumenTtdDao;
import id.ac.tazkia.spm.dao.KaryawanDao;
import id.ac.tazkia.spm.entity.*;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Controller @Slf4j
public class LegalitasController {
    @Autowired
    private DokumenDao dokumenDao;

    @Value("${upload.docLegalitas}")
    private String uploadLegalitas;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private DokumenTtdDao dokumenTddDao;

    @GetMapping("/legalitas")
    public String getLegilitas(Model model, @PageableDefault Pageable pageable, @RequestParam(required = false) String search){
        model.addAttribute("dropdown", "active");
        model.addAttribute("legalitas", "active");
        if (StringUtils.hasText(search)){
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisAndNamaDokumenContainingIgnoreCase
                            (StatusRecord.AKTIF, JenisDokumen.LEGALITAS, search, pageable));
        }else {
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisOrderByTahunDesc(StatusRecord.AKTIF, JenisDokumen.LEGALITAS, pageable));
        }
        return "legalitas/list";
    }

    @GetMapping("/legalitas/add")
    public String formLegilitas(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("legalitas", "active");
        model.addAttribute("karyawan", karyawanDao.findByStatus(StatusRecord.AKTIF));
        return "legalitas/form";
    }

    @PostMapping("/legalitas/post")
    public String addLegalitas(@Valid Dokumen dokumen, Authentication authentication,
                               @RequestParam("doc") MultipartFile file,
                               RedirectAttributes attributes) throws Exception{
        User user = currentUserService.currentUser(authentication);
        if (!file.isEmpty() || file != null){
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadLegalitas + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            dokumen.setFile(idFile + "." + extension);
        }


        dokumen.setStatus(StatusRecord.AKTIF);
        dokumen.setUserInsert(user.getUsername());
        dokumen.setTanggalInput(LocalDate.now());
        dokumen.setJenis(JenisDokumen.LEGALITAS);
        dokumenDao.save(dokumen);

        attributes.addFlashAttribute("success", "Data berhasil ditambah");
        return "redirect:/legalitas";
    }

    @GetMapping("/view-doc/{legalitas}")
    public ResponseEntity<byte[]> perbaikan(@PathVariable Dokumen legalitas) throws Exception{
        String lokasiFile = uploadLegalitas + File.separator + legalitas.getFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (legalitas.getFile().toLowerCase().endsWith("jpeg") || legalitas.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(legalitas.getFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(legalitas.getFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/legalitas/delete/{legalitas}")
    public String legalitasDelete(@PathVariable Dokumen legalitas, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        legalitas.setStatus(StatusRecord.HAPUS);
        legalitas.setUserDelete(user.getUsername());
        legalitas.setTanggalDelete(LocalDate.now());
        attributes.addFlashAttribute("delete", "detele");
        dokumenDao.save(legalitas);
        return "redirect:/legalitas";
    }

    @GetMapping("/legalitas/update/{legalitas}")
    public String formEdit(Model model,@PathVariable Dokumen legalitas){
        model.addAttribute("legalitas", legalitas);
        return "legalitas/edit";
    }

    @PostMapping("/legalitas/update")
    public String updateLegalitas(@Valid Dokumen dokumen, Authentication authentication)throws Exception{
        User user = currentUserService.currentUser(authentication);
        Optional<Dokumen> dokumenOptional = dokumenDao.findById(dokumen.getId());
        Dokumen dokumenUpdate = dokumenOptional.get();
        dokumen.setFile(dokumenOptional.get().getFile());
        dokumen.setStatus(StatusRecord.AKTIF);
        dokumen.setUserInsert(dokumenOptional.get().getUserInsert());
        BeanUtils.copyProperties(dokumen, dokumenUpdate);
        dokumenUpdate.setUserUpdate(user.getUsername());
        dokumenUpdate.setTanggalUpdate(LocalDate.now());
        dokumenUpdate.setJenis(JenisDokumen.LEGALITAS);
        dokumenDao.save(dokumenUpdate);
        return "redirect:/legalitas";
    }

}
