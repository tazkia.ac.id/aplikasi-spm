package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.config.PermissionDao;
import id.ac.tazkia.spm.dao.config.RoleDao;
import id.ac.tazkia.spm.entity.config.Role;
import jakarta.validation.Valid;
import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class RoleController {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;

    @GetMapping("/role")
    public String getRole(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("user", "active");


        model.addAttribute("list", roleDao.findAll());
        return "role/list";

    }

    @GetMapping("/role/form")
    public String getForm(Model model, @RequestParam(required = false) String id){
        model.addAttribute("dropdown", "active");
        model.addAttribute("user", "active");
        model.addAttribute("listPermission", permissionDao.findAll());

        if (id == null || id.isEmpty() || id.isBlank()){
            model.addAttribute("role", new Role());
        }else {
            Role role = roleDao.findById(id).get();
            model.addAttribute("role", role);
        }
        return "role/form";
    }

    @PostMapping("/role/save")
    public String saveRole(@Valid Role role, RedirectAttributes attributes){
        roleDao.save(role);
        attributes.addFlashAttribute("success", "Berhasil disimpan");
        return "redirect:/role";
    }
}
