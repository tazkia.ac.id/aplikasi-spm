package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.KeuanganDao;
import id.ac.tazkia.spm.dto.JumlahKeuanganDto;
import id.ac.tazkia.spm.entity.Jenis;
import id.ac.tazkia.spm.entity.Keuangan;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

@Controller @Slf4j
public class KeuanganController {
    @Autowired
    private KeuanganDao keuanganDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/keuangan")
    public String getKeuangan(Model model, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);

        model.addAttribute("pendapatan", keuanganDao.findByStatusAndJenis(StatusRecord.AKTIF, Jenis.PENDAPATAN));
        model.addAttribute("jumlahPendapatan", keuanganDao.sumKeuangan(Jenis.PENDAPATAN.toString()));

        model.addAttribute("biayaOperasional", keuanganDao.findByStatusAndJenis(StatusRecord.AKTIF, Jenis.BIAYA_OPERASIONAL));
        model.addAttribute("jumlahBiayaOperasional", keuanganDao.sumKeuangan(Jenis.BIAYA_OPERASIONAL.toString()));

        Keuangan biayaPenelitian = keuanganDao.findByJenisAndStatus(Jenis.BIAYA_PENELITIAN, StatusRecord.AKTIF);
        model.addAttribute("jumlahPenelitian", keuanganDao.sumBiayaPkm(Jenis.BIAYA_PENELITIAN.toString(), Jenis.BIAYA_PKM.toString()));
        if (biayaPenelitian != null){
            model.addAttribute("biayaPenelitian", keuanganDao.findByJenisAndStatus(Jenis.BIAYA_PENELITIAN, StatusRecord.AKTIF));
        }else {
            model.addAttribute("biayaPenelitian", new Keuangan());
        }

        Keuangan biayaPkm = keuanganDao.findByJenisAndStatus(Jenis.BIAYA_PKM, StatusRecord.AKTIF);
        if (biayaPkm != null){
            model.addAttribute("biayaPkm", keuanganDao.findByJenisAndStatus(Jenis.BIAYA_PKM, StatusRecord.AKTIF));
        }else {
            model.addAttribute("biayaPkm", new Keuangan());
        }


        Keuangan biayaSdm = keuanganDao.findByJenisAndStatus(Jenis.BIAYA_SDM, StatusRecord.AKTIF);
        model.addAttribute("jumlahInvestasi", keuanganDao.sumBiayaInvestasi(Jenis.BIAYA_SDM.toString(), Jenis.BIAYA_SARANA.toString(), Jenis.BIAYA_PRASARANA.toString()));
        if (biayaSdm != null){
            model.addAttribute("biayaSdm", keuanganDao.findByJenisAndStatus(Jenis.BIAYA_SDM, StatusRecord.AKTIF));
        }else {
            model.addAttribute("biayaSdm", new Keuangan());
        }

        Keuangan biayaSarana = keuanganDao.findByJenisAndStatus(Jenis.BIAYA_SARANA, StatusRecord.AKTIF);
        if (biayaSarana != null){
            model.addAttribute("biayaSarana", keuanganDao.findByJenisAndStatus(Jenis.BIAYA_SARANA, StatusRecord.AKTIF));
        }else {
            model.addAttribute("biayaSarana", new Keuangan());
        }

        Keuangan biayaPrasarana = keuanganDao.findByJenisAndStatus(Jenis.BIAYA_PRASARANA, StatusRecord.AKTIF);
        if (biayaPrasarana != null){
            model.addAttribute("biayaPrasarana", keuanganDao.findByJenisAndStatus(Jenis.BIAYA_PRASARANA, StatusRecord.AKTIF));
        }else {
            model.addAttribute("biayaPrasarana", new Keuangan());
        }

        return "akreditasi/keuangan/list";
    }

    @GetMapping("/keuangan/form")
    public String getKeuanganForm(Model model, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        return "akreditasi/keuangan/form";
    }

    @PostMapping("/keuangan/save")
    public String saveKeuangan(@Valid Keuangan keuangan, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        // mencari rata-rata unit
        BigDecimal unitTsDua = keuangan.getUnitTsDua();
        BigDecimal unitTsSatu = keuangan.getUnitTsSatu();
        BigDecimal unitTs = keuangan.getUnitTs();
        BigDecimal totalUnit;
        BigDecimal rataUnit;

        totalUnit = unitTsDua.add(unitTsSatu);
        if (unitTs != null){
            if (BigDecimal.ZERO.equals(unitTs)){
                rataUnit = totalUnit.divide(BigDecimal.valueOf(2), RoundingMode.HALF_EVEN);
            }else {
                totalUnit = totalUnit.add(unitTs);
                rataUnit = totalUnit.divide(BigDecimal.valueOf(3), RoundingMode.HALF_EVEN);
            }
        }else {
            rataUnit = totalUnit.divide(BigDecimal.valueOf(2), RoundingMode.HALF_EVEN);
        }

        keuangan.setUnitTsRata(rataUnit);

        // mencari rata-rata Akreditasi
        BigDecimal akreditasiTsDua = keuangan.getAkreditasiTsDua();
        BigDecimal akreditasiTsSatu = keuangan.getAkreditasiTsSatu();
        BigDecimal akreditasiTs = keuangan.getAkreditasiTs();
        BigDecimal totalAkreditasi;
        BigDecimal rataAkreditasi;

        totalAkreditasi = akreditasiTsDua.add(akreditasiTsSatu);
        if (akreditasiTs != null){
            if (BigDecimal.ZERO.equals(akreditasiTs)){
                rataAkreditasi = totalAkreditasi.divide(BigDecimal.valueOf(2), RoundingMode.HALF_EVEN);
            }else {
                totalAkreditasi = totalUnit.add(akreditasiTs);
                rataAkreditasi = totalAkreditasi.divide(BigDecimal.valueOf(3), RoundingMode.HALF_EVEN);
            }
        }else {
            rataAkreditasi = totalAkreditasi.divide(BigDecimal.valueOf(2), RoundingMode.HALF_EVEN);
        }


        keuangan.setAkreditasiTsRata(rataAkreditasi);
        keuangan.setTanggalInput(LocalDate.now());
        keuangan.setUserInput(user.getUsername());
        keuangan.setStatus(StatusRecord.AKTIF);
        keuanganDao.save(keuangan);
        return "redirect:/keuangan?prodi=" + keuangan.getProdi().getId() + "&tahun=" + keuangan.getTahun();
    }
}
