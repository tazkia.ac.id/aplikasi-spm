package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.DiplomaDao;
import id.ac.tazkia.spm.entity.Diploma;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.Optional;

@Controller @Slf4j
public class DiplomaController {
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private DiplomaDao diplomaDao;

    @GetMapping("/diploma")
    public String listData(Model model, @RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        model.addAttribute("list", diplomaDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, prodi, tahun, pageable));

        return "akreditasi/diploma/list";
    }

    @PostMapping("/diploma/save")
    public String diplomaSave(@Valid Diploma diploma, Authentication authentication, RedirectAttributes attributes){
        try {
            User user = currentUserService.currentUser(authentication);
            diploma.setTanggalInput(LocalDate.now());
            diploma.setUserInput(user.getUser());
            diplomaDao.save(diploma);
            attributes.addFlashAttribute("success", "Berhasil disimpan");
        }catch (Exception e){
            log.error("error : " + e.getMessage());
            attributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/diploma?prodi=" + diploma.getProdi().getId() + "&tahun=" + diploma.getTahun();
    }

    @GetMapping("/diploma/delete")
    public String diplomaDelete(@RequestParam String id, RedirectAttributes attributes){
        Optional<Diploma> diploma = diplomaDao.findById(id);
        try{
            diplomaDao.delete(diploma.get());
            attributes.addFlashAttribute("delete", "Berhasil dihapus");
        }catch (Exception e){
            log.error("error : " + e.getMessage());
            attributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/diploma?prodi=" + diploma.get().getProdi().getId() + "&tahun=" + diploma.get().getTahun();
    }
}
