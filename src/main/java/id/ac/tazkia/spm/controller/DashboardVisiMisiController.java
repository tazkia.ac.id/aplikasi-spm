package id.ac.tazkia.spm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardVisiMisiController {

    @GetMapping("/dashboard-visi-misi")
    public String dashboardVisiMisi() {

        return "dashboard-visi-misi";
    }
}
