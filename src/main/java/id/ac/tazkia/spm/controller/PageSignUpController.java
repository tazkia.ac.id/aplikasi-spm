package id.ac.tazkia.spm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageSignUpController {
    @GetMapping("/signup")
    public String pageSignUp(){

        return "page-signup";
    }
}
