package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.MagisterTerapanDao;
import id.ac.tazkia.spm.entity.MagisterTerapan;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.SarjanaTerapan;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.Optional;

@Controller @Slf4j
public class MagisterTerapanController {
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private MagisterTerapanDao magisterTerapanDao;


    @GetMapping("/magisterTerapan")
    public String listMagisterTerapan(Model model, @RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        model.addAttribute("list", magisterTerapanDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, prodi, tahun, pageable));
        return "akreditasi/magisterTerapan/list";
    }

    @PostMapping("/magisterTerapan/save")
    public String magisterTerapanSave(@Valid MagisterTerapan magisterTerapan, Authentication authentication, RedirectAttributes attributes){
        try {
            User user = currentUserService.currentUser(authentication);
            magisterTerapan.setTanggalInput(LocalDate.now());
            magisterTerapan.setUserInput(user.getUser());
            magisterTerapanDao.save(magisterTerapan);
            attributes.addFlashAttribute("success", "Berhasil disimpan");
        }catch (Exception e){
            log.error("error", e.getMessage());
            attributes.addFlashAttribute("error", "Terjadi Kesalahan : " + e.getMessage());
        }
        return "redirect:/magisterTerapan?prodi=" + magisterTerapan.getProdi().getId() + "&tahun=" + magisterTerapan.getTahun();
    }

    @GetMapping("/magisterTerapan/delete")
    public String sarjanaTerapanDelete(@RequestParam String id, RedirectAttributes attributes, Authentication authentication){
        try{
            User user = currentUserService.currentUser(authentication);
            MagisterTerapan magisterTerapan = magisterTerapanDao.findById(id).get();
            magisterTerapan.setStatus(StatusRecord.HAPUS);
            magisterTerapan.setTanggalDelete(LocalDate.now());
            magisterTerapan.setUserDelete(user.getUsername());
            magisterTerapanDao.save(magisterTerapan);
            attributes.addFlashAttribute("delete", "Berhasil dihapus");
            return "redirect:/magisterTerapan?prodi=" + magisterTerapan.getProdi().getId() + "&tahun=" + magisterTerapan.getTahun();
        }catch (Exception e){
            log.error("error : " + e.getMessage());
            attributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/error";
        }

    }


}
