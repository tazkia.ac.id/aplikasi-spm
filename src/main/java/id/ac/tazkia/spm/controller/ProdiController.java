package id.ac.tazkia.spm.controller;


import id.ac.tazkia.spm.dao.ProdiDao;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
public class ProdiController {
    @Autowired
    private ProdiDao prodiDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/prodi")
    public String getProdi(Model model){
        model.addAttribute("prodi", "active");
        model.addAttribute("list", prodiDao.findByStatus(StatusRecord.AKTIF));
        return "prodi/list";
    }

    @GetMapping("/prodi/edit")
    public String formProdi(Model model,@RequestParam(required = false)String id){
        model.addAttribute("prodi", prodiDao.findById(id));
        return "prodi/form";
    }

    @PostMapping("/prodi/update")
    public String update(@Valid Prodi prodi, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        prodi.setUserUpdate(user.getUsername());
        prodi.setTglUpdate(LocalDate.now());
        prodiDao.save(prodi);
        return "redirect:/prodi";
    }
}
