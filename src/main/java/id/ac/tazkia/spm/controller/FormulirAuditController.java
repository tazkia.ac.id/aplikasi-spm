package id.ac.tazkia.spm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FormulirAuditController {

    @GetMapping("/formulir-audit")
    public String FormulirAudit(){

        return "setelah-login/formulir-audit";
    }
}
