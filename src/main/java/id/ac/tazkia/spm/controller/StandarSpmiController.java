package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.DokumenDao;
import id.ac.tazkia.spm.dao.KaryawanDao;
import id.ac.tazkia.spm.entity.Dokumen;
import id.ac.tazkia.spm.entity.JenisDokumen;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.io.File;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Controller
@Slf4j
public class StandarSpmiController {
    @Autowired
    private DokumenDao dokumenDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.docLegalitas}")
    private String uploadStandar;

    @GetMapping("/standarSpmi")
    public String getStandarSpmi(Model model, @PageableDefault(size = 5) Pageable pageable,
                                 @RequestParam(required = false)String search){
        model.addAttribute("dropdown", "active");
        model.addAttribute("spmi", "active");

        if (StringUtils.hasText(search)){
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisAndNamaDokumenContainingIgnoreCase
                            (StatusRecord.AKTIF, JenisDokumen.STANDAR_SPMI, search, pageable));
        }else {
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisOrderByTahunDesc(StatusRecord.AKTIF, JenisDokumen.STANDAR_SPMI, pageable));
        }
        return "standarSpmi/list";
    }

    @GetMapping("/standarSpmi/add")
    public String formStandarSpmi(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("spmi", "active");
        model.addAttribute("karyawan", karyawanDao.findByStatus(StatusRecord.AKTIF));

        return "standarSpmi/form";
    }

    @PostMapping("/standarSpmi/save")
    public String addStandarSpmi(@Valid Dokumen standarKriteria, Authentication authentication,
                             @RequestParam("doc") MultipartFile file,
                             RedirectAttributes attributes) throws Exception{
        try{
            User user = currentUserService.currentUser(authentication);
            if (!file.isEmpty() || file != null){
                String namaAsli = file.getOriginalFilename();
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }
                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = uploadStandar + File.separator;
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                file.transferTo(tujuan);

                standarKriteria.setFile(idFile + "." + extension);
            }
            standarKriteria.setStatus(StatusRecord.AKTIF);
            standarKriteria.setUserInsert(user.getUsername());
            standarKriteria.setTanggalInput(LocalDate.now());
            standarKriteria.setJenis(JenisDokumen.STANDAR_SPMI);
            dokumenDao.save(standarKriteria);

            attributes.addFlashAttribute("success", "Berhasil");

            return "redirect:/standarSpmi";
        }catch (Exception e){
            attributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/standarSpmi";
        }

    }

    @GetMapping("/standarSpmi/delete/{standar}")
    public String deleteStandarSpmi(@PathVariable Dokumen standar, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        standar.setStatus(StatusRecord.HAPUS);
        standar.setUserDelete(user.getUsername());
        standar.setTanggalDelete(LocalDate.now());
        attributes.addFlashAttribute("delete", "detele");
        dokumenDao.save(standar);
        return "redirect:/standarSpmi";
    }

    @GetMapping("/standarSpmi/update/{standarSpmi}")
    public String getUpdate(Model model,@PathVariable Dokumen standarSpmi){
        model.addAttribute("standarSpmi", standarSpmi);
        return "standarSpmi/update";
    }

    @PostMapping("/standarSpmi/update")
    public String updateLegalitas(@Valid Dokumen dokumen, Authentication authentication, RedirectAttributes attributes)throws Exception{
        try {
            User user = currentUserService.currentUser(authentication);
            Optional<Dokumen> dokumenOptional = dokumenDao.findById(dokumen.getId());
            Dokumen dokumenUpdate = dokumenOptional.get();
            dokumen.setFile(dokumenOptional.get().getFile());
            dokumen.setStatus(StatusRecord.AKTIF);
            dokumen.setUserInsert(dokumenOptional.get().getUserInsert());
            BeanUtils.copyProperties(dokumen, dokumenUpdate);
            dokumenUpdate.setUserUpdate(user.getUsername());
            dokumenUpdate.setTanggalUpdate(LocalDate.now());
            dokumenDao.save(dokumenUpdate);
            attributes.addFlashAttribute("update", "Berhasil di update");
        }catch (Exception e){
            log.error("Error : " + e.getMessage());
            attributes.addFlashAttribute("error", e.getMessage());
        }
        return "redirect:/standarSpmi";
    }
}
