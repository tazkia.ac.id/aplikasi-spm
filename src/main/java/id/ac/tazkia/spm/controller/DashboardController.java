package id.ac.tazkia.spm.controller;


import id.ac.tazkia.spm.dao.ColorDao;
import id.ac.tazkia.spm.dao.InstitutionDao;
import id.ac.tazkia.spm.dao.finance.FinanceDashboardDao;
import id.ac.tazkia.spm.dao.finance.FinanceTransaksiEnamBulanDao;
import id.ac.tazkia.spm.dao.smile.SmileJumlahDosenAktifDao;
import id.ac.tazkia.spm.dao.smile.SmileJumlahMahasiswaAktifDao;
import id.ac.tazkia.spm.dao.smile.SmilePerformanceMahasiswaDao;
import id.ac.tazkia.spm.dao.spmb.SpmbPendaftarPerProdiDao;
import id.ac.tazkia.spm.dao.spmb.SpmbPendaftarPerTahunDao;
import id.ac.tazkia.spm.dto.*;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.finance.FinanceTransaksiEnamBulan;
import id.ac.tazkia.spm.entity.smile.SmileJumlahDosenAktif;
import id.ac.tazkia.spm.entity.smile.SmileJumlahMahasiswaAktif;
import id.ac.tazkia.spm.entity.spmb.SpmbPendaftarPerProdi;
import id.ac.tazkia.spm.entity.spmb.SpmbPendaftarPerTahun;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDate;
import java.util.List;

@Controller
public class DashboardController {


    @Autowired
    private SpmbPendaftarPerProdiDao spmbPendaftarPerProdiDao;

    @Autowired
    private SpmbPendaftarPerTahunDao spmbPendaftarPerTahunDao;

    @Autowired
    private SmilePerformanceMahasiswaDao smilePerformanceMahasiswaDao;

    @Autowired
    private FinanceDashboardDao financeDashboardDao;

    @Autowired
    private SmileJumlahDosenAktifDao smileJumlahDosenAktifDao;

    @Autowired
    private SmileJumlahMahasiswaAktifDao smileJumlahMahasiswaAktifDao;

    @Autowired
    private FinanceTransaksiEnamBulanDao financeTransaksiEnamBulanDao;

    @Autowired
    private ColorDao colorDao;

    @Autowired
    private InstitutionDao institutionDao;

    @Autowired
    @Value("${finance.url}")
    private String keuanganUrl;

    @Autowired
    @Value("${hris.url}")
    private String hrisUrl;

    @Autowired
    @Value("${smile.url}")
    private String smileUrl;

    @Autowired
    @Value("${spmb.url}")
    private String spmbUrl;




    WebClient webClient1 = WebClient.builder()
            .baseUrl(keuanganUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    WebClient webClient2 = WebClient.builder()
            .baseUrl(hrisUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    WebClient webClient3 = WebClient.builder()
            .baseUrl(smileUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    WebClient webClient4 = WebClient.builder()
            .baseUrl(spmbUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();



    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("loginError", true);
        model.addAttribute("login", "active");
        return "layout";
    }

    @GetMapping("/")
    public String getStarted(){

        return "layout";
    }

    @GetMapping("/dashboard")
    public String dashboard(Model model){
        model.addAttribute("dashboard", "active");
        return "redirect:dashboard_rektor";
//        return "dashboard/index";
    }

//    @PostConstruct
//    public DashboardKeuanganDto getDashboardKeuangan() {
//        return webClient1.get()
//                .uri(keuanganUrl+"/api/dashboard/keuangan")
//                .retrieve().bodyToMono(DashboardKeuanganDto.class)
//                .block();
//    }

    @PostConstruct
    public List<JumlahEmployeeDuaDto> getJumlahEmployeeDto() {
        return webClient2.get()
                .uri(hrisUrl+"/api/jumlah-employee")
                .retrieve().bodyToFlux(JumlahEmployeeDuaDto.class)
                .collectList()
                .block();
    }






    @PostConstruct
    public List<TransaksiSetahunDuaDto> getTransaksiSetahun() {
        return webClient1.get()
                .uri(keuanganUrl+"/api/transaksi/setahun")
                .retrieve().bodyToFlux(TransaksiSetahunDuaDto.class)
                .collectList()
                .block();
    }



    @GetMapping("/api/pengajuandana/setahun")
    @ResponseBody
    public ChartTransaksiSetahunDto totalPengajuanDanaSetahun(){

        ChartTransaksiSetahunDto chartTransaksiSetahunDto = new ChartTransaksiSetahunDto();
        List<TransaksiSetahunDuaDto> transaksiSetahunDuaDtos = getTransaksiSetahun();
        for (TransaksiSetahunDuaDto api : transaksiSetahunDuaDtos){
            chartTransaksiSetahunDto.getDaftarBulan().add(api.getBulan());
            chartTransaksiSetahunDto.getDaftarTotal().add(api.getTotal());
        }

        return chartTransaksiSetahunDto;
    }


//    @PostConstruct
//    public List<KeluarMasukTransaksiDto> getTransaksiEnamBulan() {
//        return webClient1.get()
//                .uri(keuanganUrl+"/api/transaksi/enambulan")
//                .retrieve().bodyToFlux(KeluarMasukTransaksiDto.class)
//                .collectList()
//                .block();
//    }



    @GetMapping("/api/transaksi/setahun")
    @ResponseBody
    public ChartTransaksiEnamBulanDto transaksiEnamBulan(){

        ChartTransaksiEnamBulanDto chartTransaksiEnamBulanDto = new ChartTransaksiEnamBulanDto();
        List<FinanceTransaksiEnamBulan> financeTransaksiEnamBulans = financeTransaksiEnamBulanDao.findByStatusOrderByAda(StatusRecord.AKTIF);
        for (FinanceTransaksiEnamBulan api : financeTransaksiEnamBulans){
            chartTransaksiEnamBulanDto.getDaftarNama().add(api.getNama());
            chartTransaksiEnamBulanDto.getDaftarPemasukan().add(api.getPenerimaan());
            chartTransaksiEnamBulanDto.getDaftarPengeluaran().add(api.getPengeluaran());
        }

        return chartTransaksiEnamBulanDto;
    }


//    @PostConstruct
//    public List<JumlahMahasiswaAktifDto> getJumlahMahasiswaAktif() {
//        return webClient3.get()
//                .uri(smileUrl+"/api/mahasiswa/aktif")
//                .retrieve().bodyToFlux(JumlahMahasiswaAktifDto.class)
//                .collectList()
//                .block();
//    }

    @GetMapping("/api/mahasiswa/aktif")
    @ResponseBody
    public ChartMahasiswaAktifDto totalMahasiswaAktif(){

        ChartMahasiswaAktifDto chartMahasiswaAktifDto = new ChartMahasiswaAktifDto();
        List<SmileJumlahMahasiswaAktif> smileJumlahMahasiswaAktifList = smileJumlahMahasiswaAktifDao.findByStatusOrderByTotalDesc(StatusRecord.AKTIF);
        for (SmileJumlahMahasiswaAktif api : smileJumlahMahasiswaAktifList){
            chartMahasiswaAktifDto.getDaftarKodeProdi().add(api.getKodeProdi());
            chartMahasiswaAktifDto.getDaftarMale().add(api.getPria());
            chartMahasiswaAktifDto.getDaftarFemale().add(api.getWanita());
            chartMahasiswaAktifDto.getDaftarTotal().add(api.getTotal());
        }

        return chartMahasiswaAktifDto;
    }

    @GetMapping("/api/dosen/aktif")
    @ResponseBody
    public ChartDosenAktifDto totalDosenAktif(){

        ChartDosenAktifDto chartDosenAktifDto = new ChartDosenAktifDto();
        List<SmileJumlahDosenAktif> smileJumlahDosenAktifs = smileJumlahDosenAktifDao.findByStatusOrderByTotalDesc(StatusRecord.AKTIF);
        for (SmileJumlahDosenAktif api : smileJumlahDosenAktifs){
            chartDosenAktifDto.getDaftarKodeProdi().add(api.getKodeProdi());
            chartDosenAktifDto.getDaftarLb().add(api.getLb());
            chartDosenAktifDto.getDaftarHb().add(api.getHb());
            chartDosenAktifDto.getDaftarTotal().add(api.getTotal());
        }

        return chartDosenAktifDto;
    }

    @GetMapping("/api/pendaftarPerprodi")
    @ResponseBody
    public ChartPendaftarDto laporanPendaftar(){

        ChartPendaftarDto chartPendaftarDto = new ChartPendaftarDto();
        List<SpmbPendaftarPerProdi> jumlahPendaftar = spmbPendaftarPerProdiDao.findByStatusOrderByJumlahDesc(StatusRecord.AKTIF);
        Integer nomor = 1;
        for (SpmbPendaftarPerProdi api : jumlahPendaftar){
            chartPendaftarDto.getDaftarProdi().add(api.getNama());
            chartPendaftarDto.getDaftarJumlah().add(api.getJumlah());
            chartPendaftarDto.getDaftarWarna().add(colorDao.findById(nomor).getColorcol());
            nomor=nomor+1;
        }

        return chartPendaftarDto;
    }


    @GetMapping("/api/pendaftarPertahun")
    @ResponseBody
    public ChartPendaftarPertahunDto pendaftarPertahun(){

        ChartPendaftarPertahunDto chartPendaftarPertahunDto = new ChartPendaftarPertahunDto();
        List<SpmbPendaftarPerTahun> spmbPendaftarPerTahuns = spmbPendaftarPerTahunDao.findByStatusOrderByNomor(StatusRecord.AKTIF);
        for (SpmbPendaftarPerTahun api : spmbPendaftarPerTahuns){
            chartPendaftarPertahunDto.getDaftarNama().add(api.getNama());
            chartPendaftarPertahunDto.getDaftarJumlah().add(api.getJumlah());
        }

        return chartPendaftarPertahunDto;
    }


    @GetMapping("/dashboard_rektor")
    public String dashboardRektor(Model model){
        model.addAttribute("dashboardKeuangan", financeDashboardDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("jumlahEmployee", getJumlahEmployeeDto());
        model.addAttribute("performanceMahasiswa", smilePerformanceMahasiswaDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("statusAkreditasi", institutionDao.cariBatasAkreditasi());
        model.addAttribute("dashboard", "active");
        return "dashboard/dashboard_rektor";

    }


}
