package id.ac.tazkia.spm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FormulirKerjasamaController {
    @GetMapping("/formulir-kerjasama")
    public String FormulirKerjasama(){

        return "setelah-login/formulir-kerjasama";
    }
}
