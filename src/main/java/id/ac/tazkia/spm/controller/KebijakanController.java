package id.ac.tazkia.spm.controller;

import id.ac.tazkia.spm.dao.DokumenDao;
import id.ac.tazkia.spm.dao.DokumenTtdDao;
import id.ac.tazkia.spm.dao.KaryawanDao;
import id.ac.tazkia.spm.entity.*;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Controller @Slf4j
public class KebijakanController {
    @Autowired
    private DokumenDao dokumenDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.docLegalitas}")
    private String uploadLegalitas;

    @Autowired
    private DokumenTtdDao dokumenTtdDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @GetMapping("/kebijakan")
    public String getKebijakan(Model model, @PageableDefault Pageable pageable,
                               @RequestParam(required = false)String search){
        model.addAttribute("dropdown", "active");
        model.addAttribute("kebijakan", "active");
        if (StringUtils.hasText(search)){
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisAndNamaDokumenContainingIgnoreCase
                            (StatusRecord.AKTIF, JenisDokumen.KEBIJAKAN, search, pageable));
            model.addAttribute("ttd", dokumenTtdDao.findAll());
        }else {
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisOrderByTahunDesc(StatusRecord.AKTIF, JenisDokumen.KEBIJAKAN, pageable));
            model.addAttribute("ttd", dokumenTtdDao.findAll());
        }

        return "kebijakan/list";
    }

    @GetMapping("/kebijakan/add")
    public String formKebijakan(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("kebijakan", "active");
        model.addAttribute("karyawan", karyawanDao.findByStatus(StatusRecord.AKTIF));
        return "kebijakan/form";
    }

    @PostMapping("/kebijakan/post")
    public String kebijakanAdd(@Valid Dokumen dokumen, Authentication authentication,
                               @RequestParam("ttd") String[] ttd,
                               @RequestParam("doc") MultipartFile file,
                               RedirectAttributes attributes) throws Exception{
        try{
            User user = currentUserService.currentUser(authentication);
            if (!file.isEmpty() || file != null){
                String namaAsli = file.getOriginalFilename();
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }
                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = uploadLegalitas + File.separator;
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                file.transferTo(tujuan);

                dokumen.setFile(idFile + "." + extension);
            }
            dokumen.setStatus(StatusRecord.AKTIF);
            dokumen.setUserInsert(user.getUsername());
            dokumen.setTanggalInput(LocalDate.now());
            dokumen.setJenis(JenisDokumen.KEBIJAKAN);
            dokumenDao.save(dokumen);

            Optional<Dokumen> dokumenOptional = dokumenDao.findById(dokumen.getId());
            for (String data : ttd){
                Optional<Karyawan> karyawan = karyawanDao.findById(data);
                DokumenTtd dokumenTdd = new DokumenTtd();
                dokumenTdd.setDokumen(dokumenOptional.get());
                dokumenTdd.setKaryawan(karyawan.get());
                System.out.println();
                dokumenTtdDao.save(dokumenTdd);
            }

            attributes.addFlashAttribute("success", "Data berhasil ditambah");
            return "redirect:/kebijakan";
        }catch (Exception e){
            attributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/kebijakan";
        }

    }

    @GetMapping("/kebijakan/delete/{kebijakan}")
    public String legalitasDelete(@PathVariable Dokumen kebijakan, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        kebijakan.setStatus(StatusRecord.HAPUS);
        kebijakan.setUserDelete(user.getUsername());
        kebijakan.setTanggalDelete(LocalDate.now());
        attributes.addFlashAttribute("delete", "detele");
        dokumenDao.save(kebijakan);
        return "redirect:/kebijakan";
    }

    @GetMapping("/kebijakan/update/{kebijakan}")
    public String formEdit(Model model,@PathVariable Dokumen kebijakan){
        model.addAttribute("kebijakan", kebijakan);
        return "kebijakan/edit";
    }

    @PostMapping("/kebijakan/update")
    public String updateLegalitas(@Valid Dokumen dokumen, Authentication authentication)throws Exception{
        try{
            User user = currentUserService.currentUser(authentication);
            Optional<Dokumen> dokumenOptional = dokumenDao.findById(dokumen.getId());
            Dokumen dokumenUpdate = dokumenOptional.get();
            dokumen.setFile(dokumenOptional.get().getFile());
            dokumen.setStatus(StatusRecord.AKTIF);
            dokumen.setUserInsert(dokumenOptional.get().getUserInsert());
            BeanUtils.copyProperties(dokumen, dokumenUpdate);
            dokumenUpdate.setUserUpdate(user.getUsername());
            dokumenUpdate.setTanggalUpdate(LocalDate.now());
            dokumenDao.save(dokumenUpdate);
        }catch (Exception e){
            log.error("Error : " + e.getMessage());
        }
        return "redirect:/kebijakan";

    }
}
