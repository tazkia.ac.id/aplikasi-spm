package id.ac.tazkia.spm.controller;


import id.ac.tazkia.spm.dao.DosenDao;
import id.ac.tazkia.spm.dao.DosenStatusDao;
import id.ac.tazkia.spm.dao.KeahlianDao;
import id.ac.tazkia.spm.dao.ProdiDao;
import id.ac.tazkia.spm.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

@Controller @Slf4j
public class AkreditasiController {

    @Autowired
    private ProdiDao prodiDao;


    @GetMapping("/akreditasi")
    public String getAkreditasi(Model model, @RequestParam(required = false) String prodi, @RequestParam(required = false) String tahun){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        model.addAttribute("prodi", prodiDao.findByStatus(StatusRecord.AKTIF));

        if (prodi != null && tahun != null){
            Optional<Prodi> optionalProdi = prodiDao.findById(prodi);
            model.addAttribute("cek", prodi);
            model.addAttribute("namaProdi", optionalProdi.get());
            model.addAttribute("tahun", tahun);
        }
        return "akreditasi/list";
    }


}
