package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.DosenDao;
import id.ac.tazkia.spm.dao.EwmpDao;
import id.ac.tazkia.spm.entity.Ewmp;
import id.ac.tazkia.spm.entity.Keahlian;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Controller @Slf4j
public class EwmpContoller {

    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private EwmpDao ewmpDao;

    @GetMapping("/sdm/ewmp")
    public String EwmpContoller(Model model, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);

        model.addAttribute("list", ewmpDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, prodi, tahun));
        return "akreditasi/sdm/ewmp/list";
    }

    @GetMapping("/sdm/ewmp/form")
    public String ewmpForm(Model model, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);

        model.addAttribute("dosen", dosenDao.findByStatus(StatusRecord.AKTIF));
        return "akreditasi/sdm/ewmp/form";
    }

    @PostMapping("/ewmp/post")
    public String ewmpPost(@Valid Ewmp ewmp, RedirectAttributes attributes){
        ewmpDao.save(ewmp);
        attributes.addFlashAttribute("success", "Data berhasil ditambah");
        return "redirect:/sdm/ewmp?prodi=" + ewmp.getProdi().getId() + "&tahun=" + ewmp.getTahun();
    }


    @GetMapping("/ewmp/excel")
    public void downloadExcelStatusKepegawaian(HttpServletResponse response, @RequestParam Prodi prodi, @RequestParam String tahun)throws IOException {
        String[] column = {"No", "Nama Dosen", "PS yang Diakreditasi", "PS Lain di dalam PT", "PS Lain di luar PT", "Penelitian" , "PkM", "Jumlah SKS", "Rata-rata per Semester (sks)"};
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data Status Kepegawaian");

        List<Ewmp> ewmpList = ewmpDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, prodi, tahun);
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < column.length; i++){
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(column[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        int baris = 1;

        for (Ewmp data : ewmpList){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(data.getDosen().getNama());
            row.createCell(2).setCellValue(data.getPsAkreditasi());
            row.createCell(3).setCellValue(data.getPsDalamPt());
            row.createCell(4).setCellValue(data.getPsLuarPt());
            row.createCell(5).setCellValue(data.getPenelitian());
            row.createCell(6).setCellValue(data.getPkm());
            row.createCell(7).setCellValue(data.getJumlahSks());
            row.createCell(8).setCellValue(data.getRataSks());
        }

        for (int i = 0; i < column.length; i++){
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=DataEwmp"+ LocalDate.now()+".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }
}
