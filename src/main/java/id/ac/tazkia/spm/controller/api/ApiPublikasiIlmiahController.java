package id.ac.tazkia.spm.controller.api;

import id.ac.tazkia.spm.dao.PublikasiIlmiahDao;
import id.ac.tazkia.spm.dto.BaseResponseApiDto;
import id.ac.tazkia.spm.entity.KepuasanPengguna;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.PublikasiIlmiah;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@Slf4j
@RequestMapping("/api")
public class ApiPublikasiIlmiahController {
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PublikasiIlmiahDao publikasiIlmiahDao;

    @GetMapping("/get/publikasiIlmiah")
    public ResponseEntity<BaseResponseApiDto> getPublikasiIlmiah(@RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        try{

            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .data(publikasiIlmiahDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF,prodi, tahun, pageable))
                            .build()
            );

        }catch (Exception e){
            log.error("[GET Publikasi Ilmiah] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PostMapping("/post/publikasiIlmiah")
    public ResponseEntity<BaseResponseApiDto> postKepuasanPengguna(@Valid PublikasiIlmiah publikasiIlmiah,
                                                                   Authentication authentication, BindingResult bindingResult){
        try{
            User user = currentUserService.currentUser(authentication);
            if (bindingResult.hasErrors()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new BaseResponseApiDto("ERR01", "Invalid Request Body, "
                                + bindingResult.getFieldError().getDefaultMessage()));
            }
            publikasiIlmiah.setTanggalInput(LocalDate.now());
            publikasiIlmiah.setUserInput(user.getUsername());
            publikasiIlmiahDao.save(publikasiIlmiah);
            return ResponseEntity.status(HttpStatus.OK).body(
                    BaseResponseApiDto.builder()
                            .responseCode("200")
                            .responseMessage("success")
                            .build());

        }catch (Exception e){
            log.error("[POST Publikasi Ilmiah] - [ERROR] : Unable to post data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());

        }
    }

    @GetMapping("/delete/publikasiIlmiah")
    public ResponseEntity<BaseResponseApiDto> deleteKepuasanPengguna(@RequestParam String id,
                                                                     Authentication authentication){
        try{
            User user = currentUserService.currentUser(authentication);
            PublikasiIlmiah publikasiIlmiah = publikasiIlmiahDao.cariId(id);
            publikasiIlmiah.setStatus(StatusRecord.HAPUS);
            publikasiIlmiah.setTanggalDelete(LocalDate.now());
            publikasiIlmiah.setUserInput(user.getUsername());
            publikasiIlmiahDao.save(publikasiIlmiah);
            return ResponseEntity.status(HttpStatus.OK).body(
                    BaseResponseApiDto.builder()
                            .responseCode("200")
                            .responseMessage("success")
                            .build());

        }catch (Exception e){
            log.error("[Delete KepuasanPengguna] - [ERROR] : Unable to post data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());

        }
    }
}
