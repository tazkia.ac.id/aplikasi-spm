package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.entity.Jenis;
import id.ac.tazkia.spm.entity.Prodi;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PublikasiIlmiahController {

    @GetMapping("/publikasiIlmiah")
    public String getPublikasiIlmiah(Model model, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        return "akreditasi/publikasiIlmiah/list";
    }
}
