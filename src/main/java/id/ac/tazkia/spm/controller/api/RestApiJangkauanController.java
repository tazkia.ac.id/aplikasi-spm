package id.ac.tazkia.spm.controller.api;

import id.ac.tazkia.spm.dao.JangkauanLulusanDao;
import id.ac.tazkia.spm.dto.BaseResponseApiDto;
import id.ac.tazkia.spm.entity.JangkauanLulusan;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController @Slf4j
@RequestMapping("/api")
public class RestApiJangkauanController {
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private JangkauanLulusanDao jangkauanLulusanDao;

    @GetMapping("/get/jangkauan")
    public ResponseEntity<BaseResponseApiDto> getJangkauanLulusan(@RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        try{

            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .data(jangkauanLulusanDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF,prodi, tahun, pageable))
                            .build()
            );

        }catch (Exception e){
            log.error("[GET JANGKAUAN LULUSAN] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PostMapping("/post/jangkauan")
    public ResponseEntity<BaseResponseApiDto> postJangkauanLulusan(@Valid JangkauanLulusan jangkauanLulusan,
                                                                   Authentication authentication, BindingResult bindingResult){
        try{
            User user = currentUserService.currentUser(authentication);
            if (bindingResult.hasErrors()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new BaseResponseApiDto("ERR01", "Invalid Request Body, "
                                + bindingResult.getFieldError().getDefaultMessage()));
            }
            jangkauanLulusan.setTanggalInput(LocalDate.now());
            jangkauanLulusan.setUserInput(user.getUsername());
            jangkauanLulusanDao.save(jangkauanLulusan);
            return ResponseEntity.status(HttpStatus.OK).body(
                    BaseResponseApiDto.builder()
                            .responseCode("200")
                            .responseMessage("success")
                            .build());

        }catch (Exception e){
            log.error("[POST JANGKAUAN LULUSAN] - [ERROR] : Unable to post data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());

        }
    }

    @GetMapping("/delete/jangkauan")
    public ResponseEntity<BaseResponseApiDto> deleteJangkauanLulusan(@RequestParam String id,
                                                                   Authentication authentication){
        try{
            User user = currentUserService.currentUser(authentication);
            JangkauanLulusan jangkauanLulusan = jangkauanLulusanDao.cariId(id);
            jangkauanLulusan.setStatus(StatusRecord.HAPUS);
            jangkauanLulusan.setTanggalDelete(LocalDate.now());
            jangkauanLulusan.setUserInput(user.getUsername());
            jangkauanLulusanDao.save(jangkauanLulusan);
            return ResponseEntity.status(HttpStatus.OK).body(
                    BaseResponseApiDto.builder()
                            .responseCode("200")
                            .responseMessage("success")
                            .build());

        }catch (Exception e){
            log.error("[DELETE JANGKAUAN LULUSAN] - [ERROR] : Unable to post data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());

        }
    }

}
