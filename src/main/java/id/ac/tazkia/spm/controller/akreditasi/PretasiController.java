package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.PrestasiDao;
import id.ac.tazkia.spm.entity.Prestasi;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;

@Controller @Slf4j
public class PretasiController {

    @Autowired
    private PrestasiDao prestasiDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/prestasi")
    public String getPrestasi(Model model, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        model.addAttribute("list", prestasiDao.findByStatusAndTahunAndProdi(StatusRecord.AKTIF, tahun, prodi));
        return "akreditasi/prestasi/list";
    }

    @GetMapping("/prestasi/form")
    public String getPrestasiForm(Model model, @RequestParam(required = false) String id,@RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);

        if (StringUtils.hasText(id)){
            Prestasi prestasi = prestasiDao.findById(id).get();
            model.addAttribute("prestasi", prestasi);
        }else {
            model.addAttribute("prestasi", new Prestasi());
        }
        return "akreditasi/prestasi/form";
    }

    @PostMapping("/prestasi/save")
    public String savePrestasi(@Valid Prestasi prestasi, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        try{
            prestasi.setTanggalInput(LocalDate.now());
            prestasi.setUserInput(user.getUser());
            prestasiDao.save(prestasi);
            attributes.addFlashAttribute("success", "Data Berhasil Disimpan");
        }catch (Exception e){
            log.error("error : ", e.getMessage());
            attributes.addFlashAttribute("error", "Terjadi Kesalahan : " + e.getMessage());
        }

        return "redirect:/prestasi?prodi=" + prestasi.getProdi().getId() + "&tahun=" + prestasi.getTahun();
    }

    @GetMapping("/prestasi/delete")
    public String deletePrestasi(@RequestParam Prestasi prestasi, RedirectAttributes attributes){
        try{
            prestasi.setStatus(StatusRecord.HAPUS);
            prestasiDao.save(prestasi);
            attributes.addFlashAttribute("delete", "Data Berhasil Dihapus");
        }catch (Exception e){
            log.error("error : ", e.getMessage());
            attributes.addFlashAttribute("error", "Terjadi Kesalahan : " + e.getMessage());
        }
        return "redirect:/prestasi?prodi=" + prestasi.getProdi().getId() + "&tahun=" + prestasi.getTahun();
    }
}
