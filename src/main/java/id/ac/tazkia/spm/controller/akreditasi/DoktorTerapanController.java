package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.DoktorTerapanDao;
import id.ac.tazkia.spm.entity.DoktorTerapan;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.SarjanaTerapan;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.Optional;

@Controller @Slf4j
public class DoktorTerapanController {
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private DoktorTerapanDao doktorTerapanDao;

    @GetMapping("/doktorTerapan")
    public String getDoktorTerapan(Model model, @RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        model.addAttribute("list", doktorTerapanDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, prodi, tahun, pageable));
        return "akreditasi/doktorTerapan/list";
    }

    @PostMapping("/doktorTerapan/save")
    public String doktorTerapanSave(@Valid DoktorTerapan doktorTerapan, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        try{
            doktorTerapan.setTanggalInput(LocalDate.now());
            doktorTerapan.setUserInput(user.getUser());
            doktorTerapanDao.save(doktorTerapan);
            attributes.addFlashAttribute("success", "Berhasil Disimpan");
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Terjadi Kesalahan : " + e.getMessage());
        }
        return "redirect:/doktorTerapan?prodi=" + doktorTerapan.getProdi().getId() + "&tahun=" + doktorTerapan.getTahun();
    }

    @GetMapping("/doktorTerapan/delete")
    public String sarjanaTerapanDelete(@RequestParam String id, RedirectAttributes attributes, Authentication authentication){
        try{
            User user = currentUserService.currentUser(authentication);
            DoktorTerapan doktorTerapan = doktorTerapanDao.findById(id).get();
            doktorTerapan.setStatus(StatusRecord.HAPUS);
            doktorTerapan.setTanggalDelete(LocalDate.now());
            doktorTerapan.setUserDelete(user.getUser());
            doktorTerapanDao.save(doktorTerapan);
            attributes.addFlashAttribute("delete", "Berhasil dihapus");
            return "redirect:/doktorTerapan?prodi=" + doktorTerapan.getProdi().getId() + "&tahun=" + doktorTerapan.getTahun();
        }catch (Exception e){
            log.error("error : " + e.getMessage());
            attributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/error";
        }
    }
}
