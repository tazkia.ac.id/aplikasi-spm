package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.KaryawanDao;
import id.ac.tazkia.spm.dao.TendikDao;
import id.ac.tazkia.spm.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Controller @Slf4j
public class TendikController {

    @Autowired
    private TendikDao tendikDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Value("${upload.docDiploma}")
    private String docDiploma;

    @Value("${upload.docIjazahS1}")
    private String docIjazahS1;

    @Value("${upload.docIjazahS2}")
    private String docIjazahS2;

    @Value("${upload.docIjazahS3}")
    private String docIjazahS3;

    @Value("${upload.sertifikatKompetensi}")
    private String sertifikatKompetensi;

    @GetMapping("/sdm/tendik")
    public String getTendik(Model model, @RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault(size = 25)Pageable pageable){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);

        model.addAttribute("list", tendikDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, prodi, tahun, pageable));
        return "akreditasi/sdm/tendik/list";
    }

    @GetMapping("/sdm/tendik/form")
    public String formTendik(Model model, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);

        model.addAttribute("karyawan", karyawanDao.findByStatus(StatusRecord.AKTIF));
        return "akreditasi/sdm/tendik/form";
    }

    @PostMapping("/sdm/tendik/add")
    public String addTendik(@Valid Tendik tendik, @RequestParam("fileDoc1") MultipartFile file1,
                            @RequestParam("fileDoc2") MultipartFile file2,
                            @RequestParam("fileDoc3")MultipartFile file3,
                            @RequestParam("fileDoc4")MultipartFile file4,
                            @RequestParam("fileDoc5")MultipartFile file5,
                            RedirectAttributes attributes) throws Exception{

        if (!file1.isEmpty() || file1 != null){
            String namaAsli = file1.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docDiploma + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file1.transferTo(tujuan);

            tendik.setDiplomaFile(idFile + "." + extension);
        }

        if (!file2.isEmpty() || file2 != null){
            String namaAsli = file2.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docIjazahS1 + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file2.transferTo(tujuan);

            tendik.setSarjanaFile(idFile + "." + extension);
        }

        if (!file3.isEmpty() || file3 != null){
            String namaAsli = file3.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docIjazahS2 + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file3.transferTo(tujuan);

            tendik.setMagisterFile(idFile + "." + extension);
        }

        if (!file4.isEmpty() || file4 != null){
            String namaAsli = file4.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = docIjazahS3 + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file4.transferTo(tujuan);

            tendik.setDoktorFile(idFile + "." + extension);
        }

        if (!file5.isEmpty() || file5 != null){
            String namaAsli = file4.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = sertifikatKompetensi + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file5.transferTo(tujuan);

            tendik.setSertifikatFile(idFile + "." + extension);
        }

        tendikDao.save(tendik);

        attributes.addFlashAttribute("success", "Data berhasil ditambah");
        return "redirect:/sdm/tendik?prodi=" + tendik.getProdi().getId() + "&tahun=" + tendik.getTahun();
    }

    @GetMapping("/view-doc/diploma/{doc}")
    public ResponseEntity<byte[]> viewDocDiploma(@PathVariable Tendik doc) throws Exception{
        String lokasiFile = docDiploma + File.separator + doc.getDiplomaFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getDiplomaFile().toLowerCase().endsWith("jpeg") || doc.getDiplomaFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getDiplomaFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getDiplomaFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/sarjana/{doc}")
    public ResponseEntity<byte[]> viewDocSarjana(@PathVariable Tendik doc) throws Exception{
        String lokasiFile = docIjazahS1 + File.separator + doc.getSarjanaFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getSarjanaFile().toLowerCase().endsWith("jpeg") || doc.getSarjanaFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getSarjanaFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getSarjanaFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/magister/{doc}")
    public ResponseEntity<byte[]> viewDocMagister(@PathVariable Tendik doc) throws Exception{
        String lokasiFile = docIjazahS2 + File.separator + doc.getMagisterFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getMagisterFile().toLowerCase().endsWith("jpeg") || doc.getMagisterFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getMagisterFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getMagisterFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/doctor/{doc}")
    public ResponseEntity<byte[]> viewDocDoctor(@PathVariable Tendik doc) throws Exception{
        String lokasiFile = docIjazahS3 + File.separator + doc.getDoktorFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getDoktorFile().toLowerCase().endsWith("jpeg") || doc.getDoktorFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getDoktorFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getDoktorFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/view-doc/sertifikat/{doc}")
    public ResponseEntity<byte[]> viewDocSertifikat(@PathVariable Tendik doc) throws Exception{
        String lokasiFile = sertifikatKompetensi + File.separator + doc.getSertifikatFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getSertifikatFile().toLowerCase().endsWith("jpeg") || doc.getSertifikatFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getSertifikatFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getSertifikatFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/tendik/excel")
    public void downloadExcelStatusKepegawaian(HttpServletResponse response, @RequestParam Prodi prodi, @RequestParam String tahun)throws IOException {
        String[] column = {"No", "Nama", "Status", "Jabatan", "Diploma", "Sarjana/ Sarjana Terapan" , "Magister/ Magister Terapan", "Doktor/ Doktor Terapan", "Sertifikat Kompetensi/ Profesi/Industri"};
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data Status Kepegawaian");

        List<Tendik> tendikList = tendikDao.findByStatusAndTahunAndProdi(StatusRecord.AKTIF, tahun, prodi);
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < column.length; i++){
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(column[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        int baris = 1;

        for (Tendik data : tendikList){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(data.getKaryawan().getNama());
            row.createCell(2).setCellValue(data.getStatusTendik());
            row.createCell(3).setCellValue("-");
            row.createCell(4).setCellValue(data.getDiploma());
            row.createCell(5).setCellValue(data.getSarjana());
            row.createCell(6).setCellValue(data.getMagister());
            row.createCell(7).setCellValue(data.getDoktor());
            row.createCell(8).setCellValue(data.getSertifikat());
        }

        for (int i = 0; i < column.length; i++){
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=DataTendik"+ LocalDate.now()+".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }
}
