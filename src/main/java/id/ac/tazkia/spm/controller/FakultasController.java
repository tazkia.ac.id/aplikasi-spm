package id.ac.tazkia.spm.controller;


import id.ac.tazkia.spm.dao.FakultasDao;
import id.ac.tazkia.spm.dao.InstitutionDao;
import id.ac.tazkia.spm.entity.Fakultas;
import id.ac.tazkia.spm.entity.Institution;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.UUID;

@Controller @Slf4j
public class FakultasController {
    @Autowired
    private InstitutionDao institutionDao;

    @Value("${upload.docLegalitas}")
    private String uploadLegalitas;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private FakultasDao fakultasDao;

    @GetMapping("/fakultas")
    public String getFakultas(Model model, @PageableDefault(size = 10)Pageable pageable){
        model.addAttribute("dropdown", "active");
        model.addAttribute("fakultas", "active");
        model.addAttribute("list", fakultasDao.findByStatus(StatusRecord.AKTIF, pageable));
        return "fakultas/list";
    }

    @GetMapping("/fakultas/form")
    public String formFakultas(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("fakultas", "active");
        model.addAttribute("institusi", institutionDao.findByStatus(StatusRecord.AKTIF));
        return "fakultas/form";
    }

    @PostMapping("/fakultas/add")
    public String postFakultas(@Valid Fakultas fakultas, @RequestParam("doc") MultipartFile file,
                               Authentication authentication,
                               RedirectAttributes attributes) throws Exception{

        User user = currentUserService.currentUser(authentication);
        if (!file.isEmpty() || file != null){
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadLegalitas + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            fakultas.setFile(idFile + "." + extension);
        }

        fakultas.setUserInsert(user.getUsername());
        fakultas.setTanggalInput(LocalDate.now());
        fakultas.setStatus(StatusRecord.AKTIF);
        fakultasDao.save(fakultas);
        attributes.addFlashAttribute("success", "Data berhasil ditambah");
        return "redirect:/fakultas";
    }

    @GetMapping("/view-doc/fakultas/{doc}")
    public ResponseEntity<byte[]> perbaikan(@PathVariable Fakultas doc) throws Exception{
        String lokasiFile = uploadLegalitas + File.separator + doc.getFile();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (doc.getFile().toLowerCase().endsWith("jpeg") || doc.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(doc.getFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(doc.getFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
