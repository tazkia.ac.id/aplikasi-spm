package id.ac.tazkia.spm.controller;


import id.ac.tazkia.spm.dao.DokumenDao;
import id.ac.tazkia.spm.dao.DokumenTtdDao;
import id.ac.tazkia.spm.dao.KaryawanDao;
import id.ac.tazkia.spm.entity.*;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Controller @Slf4j
public class StandarKriteriaController {
    @Autowired
    private DokumenDao dokumenDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.docLegalitas}")
    private String uploadStandar;

    @Autowired
    private DokumenTtdDao dokumenTtdDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @GetMapping("/standar")
    public String getStandar(Model model,@PageableDefault Pageable pageable,
                             @RequestParam(required = false) String search){
        model.addAttribute("dropdown", "active");
        model.addAttribute("standar", "active");

        if (StringUtils.hasText(search)){
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisAndNamaDokumenContainingIgnoreCase
                            (StatusRecord.AKTIF, JenisDokumen.STANDAR, search, pageable));
        }else {
            model.addAttribute("list",
                    dokumenDao.findByStatusAndJenisOrderByTahunDesc(StatusRecord.AKTIF, JenisDokumen.STANDAR, pageable));
        }
        return "standarKriteria/list";
    }

    @GetMapping("/standar/form")
    public String formStandar(Model model){
        model.addAttribute("dropdown", "active");
        model.addAttribute("standar", "active");
        model.addAttribute("karyawan", karyawanDao.findByStatus(StatusRecord.AKTIF));
        return "standarKriteria/form";
    }

    @PostMapping("/standar/add")
    public String addStandar(@Valid Dokumen standarKriteria, Authentication authentication,
                             @RequestParam("doc") MultipartFile file,
                             RedirectAttributes attributes) throws Exception{
        User user = currentUserService.currentUser(authentication);
        if (!file.isEmpty() || file != null){
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadStandar + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            standarKriteria.setFile(idFile + "." + extension);
        }
        standarKriteria.setStatus(StatusRecord.AKTIF);
        standarKriteria.setUserInsert(user.getUsername());
        standarKriteria.setTanggalInput(LocalDate.now());
        standarKriteria.setJenis(JenisDokumen.STANDAR);
        dokumenDao.save(standarKriteria);
        attributes.addFlashAttribute("success", "Berhasil");
        return "redirect:/standar";
    }

    @GetMapping("/standar/delete/{standar}")
    public String legalitasDelete(@PathVariable Dokumen standar, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        standar.setStatus(StatusRecord.HAPUS);
        standar.setUserDelete(user.getUsername());
        standar.setTanggalDelete(LocalDate.now());
        attributes.addFlashAttribute("delete", "detele");
        dokumenDao.save(standar);
        return "redirect:/standar";
    }

    @GetMapping("/standar/update/{standar}")
    public String formEdit(Model model,@PathVariable Dokumen standar){
        model.addAttribute("standar", standar);
        return "standarKriteria/edit";
    }

    @PostMapping("/standar/update")
    public String updateLegalitas(@Valid Dokumen dokumen, Authentication authentication)throws Exception{
        User user = currentUserService.currentUser(authentication);
        Optional<Dokumen> dokumenOptional = dokumenDao.findById(dokumen.getId());
        Dokumen dokumenUpdate = dokumenOptional.get();
        dokumen.setFile(dokumenOptional.get().getFile());
        dokumen.setStatus(StatusRecord.AKTIF);
        dokumen.setUserInsert(dokumenOptional.get().getUserInsert());
        BeanUtils.copyProperties(dokumen, dokumenUpdate);
        dokumenUpdate.setUserUpdate(user.getUsername());
        dokumenUpdate.setTanggalUpdate(LocalDate.now());
        dokumenUpdate.setJenis(JenisDokumen.STANDAR);
        dokumenDao.save(dokumenUpdate);
        return "redirect:/standar";
    }
}
