package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.BidangKerjaDao;
import id.ac.tazkia.spm.entity.BidangKerja;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.WaktuTungguLulus;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
public class KesesuaianBidangKerjaController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private BidangKerjaDao bidangKerjaDao;
    @GetMapping("/bidangKerja")
    public String kesesuaianBidangKerja(Model model, @RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);

        model.addAttribute("list", bidangKerjaDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, prodi, tahun, pageable));
        return "akreditasi/bidangKerja/list";
    }

    @PostMapping("/bidangKerja/post")
    public String bidangKerja(@Valid BidangKerja bidangKerja, Authentication authentication, RedirectAttributes attributes){
        try{
            User user = currentUserService.currentUser(authentication);
            bidangKerja.setUserInput(user.getUsername());
            bidangKerja.setTanggalInput(LocalDate.now());
            bidangKerjaDao.save(bidangKerja);
            attributes.addFlashAttribute("success", "Berhasil Disimpan");
            return "redirect:/bidangKerja?prodi=" + bidangKerja.getProdi().getId() + "&tahun=" + bidangKerja.getTahun();
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Terjadi Kesalahan : " + e.getMessage());
            return "redirect:/bidangKerja?prodi=" + bidangKerja.getProdi().getId() + "&tahun=" + bidangKerja.getTahun();
        }
    }
}
