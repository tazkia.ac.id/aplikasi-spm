package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.SarjanaTerapanDao;
import id.ac.tazkia.spm.entity.Diploma;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.SarjanaTerapan;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.config.User;
import id.ac.tazkia.spm.service.CurrentUserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.Optional;

@Controller @Slf4j
public class SarjanaTerapanController {
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private SarjanaTerapanDao sarjanaTerapanDao;

    @GetMapping("/sarjanaTerapan")
    public String listSarjanaTerapan(Model model, @RequestParam Prodi prodi, @RequestParam String tahun, @PageableDefault Pageable pageable){
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        model.addAttribute("list", sarjanaTerapanDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, prodi, tahun, pageable));
        return "akreditasi/sarjanaTerapan/list";
    }

    @PostMapping("/sarjanaTerapan/save")
    public String sarjanaTerapanSave(@Valid SarjanaTerapan sarjanaTerapan, Authentication authentication, RedirectAttributes attributes){
        User user = currentUserService.currentUser(authentication);
        try{
            sarjanaTerapan.setTanggalInput(LocalDate.now());
            sarjanaTerapan.setUserInput(user.getUser());
            sarjanaTerapanDao.save(sarjanaTerapan);
            attributes.addFlashAttribute("success", "Berhasil Disimpan");
            return "redirect:/sarjanaTerapan?prodi=" + sarjanaTerapan.getProdi().getId() + "&tahun=" + sarjanaTerapan.getTahun();

        }catch (Exception e){
            attributes.addFlashAttribute("error", "Terjadi Kesalahan : " + e.getMessage());
            return "redirect:/sarjanaTerapan?prodi=" + sarjanaTerapan.getProdi().getId() + "&tahun=" + sarjanaTerapan.getTahun();

        }
    }

    @GetMapping("/sarjanaTerapan/delete")
    public String sarjanaTerapanDelete(@RequestParam String id, RedirectAttributes attributes){
        try{
            Optional<SarjanaTerapan> sarjanaTerapan = sarjanaTerapanDao.findById(id);
            sarjanaTerapanDao.delete(sarjanaTerapan.get());
            attributes.addFlashAttribute("delete", "Berhasil dihapus");
            return "redirect:/sarjanaTerapan?prodi=" + sarjanaTerapan.get().getProdi().getId() + "&tahun=" + sarjanaTerapan.get().getTahun();

        }catch (Exception e){
            log.error("error : " + e.getMessage());
            attributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/error";
        }
    }
}
