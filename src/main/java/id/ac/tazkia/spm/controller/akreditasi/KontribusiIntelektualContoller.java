package id.ac.tazkia.spm.controller.akreditasi;

import id.ac.tazkia.spm.dao.DosenDao;
import id.ac.tazkia.spm.dao.KontribusiIntelektualDao;
import id.ac.tazkia.spm.dao.MatakuliahDao;
import id.ac.tazkia.spm.dao.ProdiDao;
import id.ac.tazkia.spm.entity.KontribusiIntelektual;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.util.Optional;

@Controller @Slf4j
public class KontribusiIntelektualContoller {

    @Autowired
    private ProdiDao prodiDao;

    @Autowired
    private KontribusiIntelektualDao kontribusiIntelektualDao;

    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private MatakuliahDao matakuliahDao;

    @GetMapping("/sdm/kontribusi")
    public String listKontribusiDosen(Model model, @RequestParam String prodi, @RequestParam String tahun, @PageableDefault(size = 30) Pageable pageable){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        Optional<Prodi> optionalProdi = prodiDao.findById(prodi);
        model.addAttribute("namaProdi", optionalProdi.get());
        model.addAttribute("tahun", tahun);

        model.addAttribute("list", kontribusiIntelektualDao.findByStatusAndProdiAndTahun(StatusRecord.AKTIF, optionalProdi.get(), tahun, pageable));
        return "akreditasi/sdm/kontribusi/list";
    }

    @GetMapping("/sdm/kontribusi/add")
    public String postKontribusiDosen(Model model, @RequestParam Prodi prodi, @RequestParam String tahun){
        model.addAttribute("dropdown", "active");
        model.addAttribute("akreditasi", "active");
        model.addAttribute("prodi", prodi);
        model.addAttribute("tahun", tahun);
        model.addAttribute("dosen", dosenDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("matakuliah", matakuliahDao.findByStatus(StatusRecord.AKTIF));
        return "akreditasi/sdm/kontribusi/form";
    }

    @PostMapping("/sdm/kontribusi/post")
    public String postKontribusi(@Valid KontribusiIntelektual kontribusiIntelektual, RedirectAttributes attributes){
        kontribusiIntelektualDao.save(kontribusiIntelektual);
        attributes.addFlashAttribute("success", "Data berhasil ditambah");
        return "redirect:/sdm/kontribusi?prodi=" + kontribusiIntelektual.getProdi().getId() + "&tahun=" + kontribusiIntelektual.getTahun();
    }
}
