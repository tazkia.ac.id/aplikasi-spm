package id.ac.tazkia.spm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageSignInController {

    @GetMapping("/signin")
    public String pageSignIn(){

        return "page-signin";
    }
}
