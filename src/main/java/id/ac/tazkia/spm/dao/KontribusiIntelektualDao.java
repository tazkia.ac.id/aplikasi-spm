package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.KontribusiIntelektual;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface KontribusiIntelektualDao extends PagingAndSortingRepository<KontribusiIntelektual, String>, CrudRepository<KontribusiIntelektual, String> {
    Page<KontribusiIntelektual> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);
}
