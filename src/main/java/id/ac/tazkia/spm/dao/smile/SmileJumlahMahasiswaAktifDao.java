package id.ac.tazkia.spm.dao.smile;

import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.smile.SmileJumlahMahasiswaAktif;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SmileJumlahMahasiswaAktifDao extends PagingAndSortingRepository<SmileJumlahMahasiswaAktif, String>, CrudRepository<SmileJumlahMahasiswaAktif, String> {

    List<SmileJumlahMahasiswaAktif> findByStatusOrderByTotalDesc(StatusRecord statusRecord);

}
