package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.DokumenTtd;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DokumenTtdDao extends PagingAndSortingRepository<DokumenTtd, String>, CrudRepository<DokumenTtd, String> {
}
