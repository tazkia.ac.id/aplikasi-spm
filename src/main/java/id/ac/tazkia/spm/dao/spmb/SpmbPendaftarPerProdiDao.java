package id.ac.tazkia.spm.dao.spmb;

import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.spmb.SpmbPendaftarPerProdi;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SpmbPendaftarPerProdiDao extends CrudRepository<SpmbPendaftarPerProdi, String>, PagingAndSortingRepository<SpmbPendaftarPerProdi, String> {

    List<SpmbPendaftarPerProdi> findByStatusOrderByJumlahDesc(StatusRecord statusRecord);

}
