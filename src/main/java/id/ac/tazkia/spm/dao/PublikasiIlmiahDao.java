package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.KepuasanPengguna;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.PublikasiIlmiah;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PublikasiIlmiahDao extends PagingAndSortingRepository<PublikasiIlmiah, String>, CrudRepository<PublikasiIlmiah, String> {
    Page<PublikasiIlmiah> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);

    @Query(value = "select * from publikasi_ilmiah where id = ?1", nativeQuery = true)
    PublikasiIlmiah cariId(String id);
}
