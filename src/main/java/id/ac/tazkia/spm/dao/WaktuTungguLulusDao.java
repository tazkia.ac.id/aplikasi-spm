package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Jenis;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.WaktuTungguLulus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WaktuTungguLulusDao extends PagingAndSortingRepository<WaktuTungguLulus, String>, CrudRepository<WaktuTungguLulus, String> {
    Page<WaktuTungguLulus> findByStatusAndProdiAndTahunAndJenis(StatusRecord statusRecord, Prodi prodi, String tahun, Jenis jenis, Pageable pageable);
}
