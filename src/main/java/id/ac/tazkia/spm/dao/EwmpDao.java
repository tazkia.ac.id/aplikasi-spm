package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Ewmp;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EwmpDao extends PagingAndSortingRepository<Ewmp, String>, CrudRepository<Ewmp, String> {
    List<Ewmp> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun);
}
