package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.DosenStatus;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DosenStatusDao extends PagingAndSortingRepository<DosenStatus, String>, CrudRepository<DosenStatus, String> {
    Page<DosenStatus> findByStatus(StatusRecord statusRecord, Pageable pageable);

    Page<DosenStatus> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);

    List<DosenStatus> findByStatusAndTahunAndProdi(StatusRecord statusRecord, String tahun, Prodi prodi);
}
