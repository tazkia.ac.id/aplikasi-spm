package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProdiDao extends PagingAndSortingRepository<Prodi, String>, CrudRepository<Prodi, String> {
    List<Prodi> findByStatus(StatusRecord statusRecord);
}
