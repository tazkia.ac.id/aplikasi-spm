package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.KepuasanPengguna;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface KepuasanPenggunaDao extends PagingAndSortingRepository<KepuasanPengguna, String>, CrudRepository<KepuasanPengguna, String> {
    Page<KepuasanPengguna> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);

    @Query(value = "select * from kepuasan_pengguna where id = ?1", nativeQuery = true)
    KepuasanPengguna cariId(String id);
}
