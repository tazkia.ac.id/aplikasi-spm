package id.ac.tazkia.spm.dao.spmb;

import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.spmb.SpmbPendaftarPerTahun;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SpmbPendaftarPerTahunDao extends PagingAndSortingRepository<SpmbPendaftarPerTahun, String>, CrudRepository<SpmbPendaftarPerTahun, String> {

    List<SpmbPendaftarPerTahun> findByStatusOrderByJumlahDesc(StatusRecord statusRecord);

    List<SpmbPendaftarPerTahun> findByStatusOrderByNomor(StatusRecord statusRecord);

}
