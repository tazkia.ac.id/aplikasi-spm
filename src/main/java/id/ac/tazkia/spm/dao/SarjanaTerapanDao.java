package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.SarjanaTerapan;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SarjanaTerapanDao extends PagingAndSortingRepository<SarjanaTerapan, String>, CrudRepository<SarjanaTerapan, String> {
    Page<SarjanaTerapan> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);
}
