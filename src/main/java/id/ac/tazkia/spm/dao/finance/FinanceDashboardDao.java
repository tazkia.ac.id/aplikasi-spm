package id.ac.tazkia.spm.dao.finance;

import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.finance.FinanceDashboard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FinanceDashboardDao extends PagingAndSortingRepository<FinanceDashboard, String>, CrudRepository<FinanceDashboard, String> {

    FinanceDashboard findByStatus(StatusRecord statusRecord);

}
