package id.ac.tazkia.spm.dao.config;

import id.ac.tazkia.spm.entity.config.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserDao extends PagingAndSortingRepository<User, String>, CrudRepository<User, String> {
    User findByUsername(String username);
    Page<User> findByActive(Boolean active, Pageable pageable);

    Page<User> findByUsernameContainingIgnoreCase(String seacrh, Pageable pageable);
}
