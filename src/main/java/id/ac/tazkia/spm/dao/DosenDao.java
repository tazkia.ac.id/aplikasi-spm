package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Dosen;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DosenDao extends PagingAndSortingRepository<Dosen, String> {
    List<Dosen> findByStatus(StatusRecord statusRecord);

}
