package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Keahlian;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KeahlianDao extends PagingAndSortingRepository<Keahlian, String>, CrudRepository<Keahlian, String> {
    Page<Keahlian> findByStatus(StatusRecord statusRecord, Pageable pageable);

    Page<Keahlian> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);

    List<Keahlian> findByStatusAndTahunAndProdi(StatusRecord statusRecord, String tahun, Prodi prodi);
}
