package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.JangkauanLulusan;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface JangkauanLulusanDao extends PagingAndSortingRepository<JangkauanLulusan, String>, CrudRepository<JangkauanLulusan, String> {
    Page<JangkauanLulusan> findByStatusAndProdiAndTahun(StatusRecord status, Prodi prodi, String tahun, Pageable pageable);

    @Query(value = "select * from jangkauan_lulusan where id = ?1", nativeQuery = true)
    JangkauanLulusan cariId(String id);
}
