package id.ac.tazkia.spm.dao.finance;

import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.finance.FinanceTransaksiEnamBulan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface FinanceTransaksiEnamBulanDao extends PagingAndSortingRepository<FinanceTransaksiEnamBulan, String>, CrudRepository<FinanceTransaksiEnamBulan, String> {

    List<FinanceTransaksiEnamBulan> findByStatusOrderByAda(StatusRecord statusRecord);

}
