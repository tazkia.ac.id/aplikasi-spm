package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Prestasi;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PrestasiDao extends PagingAndSortingRepository<Prestasi, String>, CrudRepository<Prestasi, String> {
    List<Prestasi>findByStatusAndTahunAndProdi(StatusRecord statusRecord, String tahun, Prodi prodi);
}
