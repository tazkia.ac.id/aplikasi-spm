package id.ac.tazkia.spm.dao.smile;

import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.smile.SmileJumlahDosenAktif;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SmileJumlahDosenAktifDao extends PagingAndSortingRepository<SmileJumlahDosenAktif, String>, CrudRepository<SmileJumlahDosenAktif, String> {

    List<SmileJumlahDosenAktif> findByStatusOrderByTotalDesc(StatusRecord statusRecord);

}
