package id.ac.tazkia.spm.dao.smile;

import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.smile.SmilePerformanceMahasiswa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SmilePerformanceMahasiswaDao extends PagingAndSortingRepository<SmilePerformanceMahasiswa, String>, CrudRepository<SmilePerformanceMahasiswa, String> {

    SmilePerformanceMahasiswa findByStatusAndTahunAndBulan(StatusRecord statusRecord, String tahun, String bulan);

    SmilePerformanceMahasiswa findByStatus(StatusRecord statusRecord);


}
