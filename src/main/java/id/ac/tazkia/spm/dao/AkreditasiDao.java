package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Akreditasi;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AkreditasiDao extends PagingAndSortingRepository<Akreditasi, String>, CrudRepository<Akreditasi, String> {
    List<Akreditasi> findByNamaProgramStudiOrderByNamaProgramStudi(String namaProgramStudi);
}
