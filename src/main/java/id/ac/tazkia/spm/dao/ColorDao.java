package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Color;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ColorDao extends PagingAndSortingRepository<Color, String> {

    Color findById(Integer id);

}
