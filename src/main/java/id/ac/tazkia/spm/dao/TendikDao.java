package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import id.ac.tazkia.spm.entity.Tendik;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TendikDao extends PagingAndSortingRepository<Tendik, String>, CrudRepository<Tendik, String> {
    Page<Tendik> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);

    List<Tendik> findByStatusAndTahunAndProdi(StatusRecord statusRecord, String tahun, Prodi prodi);
}
