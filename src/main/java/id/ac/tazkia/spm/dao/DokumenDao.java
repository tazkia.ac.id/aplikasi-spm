package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Dokumen;
import id.ac.tazkia.spm.entity.JenisDokumen;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface DokumenDao extends PagingAndSortingRepository<Dokumen, String>, CrudRepository<Dokumen, String> {
    Page<Dokumen> findByStatusAndJenisOrderByTahunDesc(StatusRecord statusRecord, JenisDokumen jenis, Pageable pageable);

    Page<Dokumen> findByStatusAndJenisAndNamaDokumenContainingIgnoreCase
            (StatusRecord statusRecord, JenisDokumen jenisDokumen,String nama, Pageable pageable);

}
