package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Karyawan;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KaryawanDao extends PagingAndSortingRepository<Karyawan, String>, CrudRepository<Karyawan, String> {
    List<Karyawan> findByStatus(StatusRecord statusRecord);
}
