package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.dto.KaryaIlmiahDto;
import id.ac.tazkia.spm.entity.KaryaIlmiah;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.PublikasiIlmiah;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface KaryaIlmiahDao extends PagingAndSortingRepository<KaryaIlmiah, String>, CrudRepository<KaryaIlmiah, String> {
    Page<KaryaIlmiah> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);

    @Query(value = "select * from karya_ilmiah where id = ?1", nativeQuery = true)
    KaryaIlmiah cariId(String id);

    @Query(value = "select a.id ,a.mahasiswa, a.judul, a.jumlah_sitasi as jumlahSitasi, a.standar from karya_ilmiah as a inner join prodi as b on a.prodi = b.id where a.id = ?1", nativeQuery = true)
    KaryaIlmiahDto getDataEdit(String id);
}
