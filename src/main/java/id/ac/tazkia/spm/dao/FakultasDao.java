package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Fakultas;
import id.ac.tazkia.spm.entity.Institution;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface FakultasDao extends PagingAndSortingRepository<Fakultas, String>, CrudRepository<Fakultas, String> {
    Page<Fakultas> findByStatus(StatusRecord statusRecord, Pageable pageable);
}
