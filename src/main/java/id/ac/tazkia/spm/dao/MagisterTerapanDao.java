package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.MagisterTerapan;
import id.ac.tazkia.spm.entity.Prodi;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MagisterTerapanDao extends PagingAndSortingRepository<MagisterTerapan, String>, CrudRepository<MagisterTerapan, String> {
    Page<MagisterTerapan> findByStatusAndProdiAndTahun(StatusRecord statusRecord, Prodi prodi, String tahun, Pageable pageable);
}
