package id.ac.tazkia.spm.dao.config;

import id.ac.tazkia.spm.entity.config.Permission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PermissionDao extends PagingAndSortingRepository<Permission, String>, CrudRepository<Permission, String> {
}
