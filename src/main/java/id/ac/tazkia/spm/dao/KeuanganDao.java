package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.dto.JumlahKeuanganDto;
import id.ac.tazkia.spm.entity.Jenis;
import id.ac.tazkia.spm.entity.Keuangan;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.awt.*;
import java.math.BigDecimal;
import java.util.List;

public interface KeuanganDao extends PagingAndSortingRepository<Keuangan, String>, CrudRepository<Keuangan, String> {
    List<Keuangan> findByStatusAndJenis(StatusRecord statusRecord, Jenis jenis);
    Keuangan findByJenisAndStatus(Jenis jenis, StatusRecord statusRecord);

    @Query(value = "select coalesce(sum(unit_ts_dua), 0) as unitTsDua, coalesce(sum(unit_ts_satu), 0) as unitTsSatu, coalesce(sum(unit_ts), 0) as unitTs, sum(unit_ts_rata) as unitTsRata,\n" +
            "sum(akreditasi_ts_dua) as akreditasiTsDua, sum(akreditasi_ts_satu) as akreditasiTsSatu, coalesce(sum(akreditasi_ts), 0) as akreditasiTs, sum(akreditasi_ts_rata) as akreditasiTsRata\n" +
            "from keuangan where jenis = ?1", nativeQuery = true)
    JumlahKeuanganDto sumKeuangan(String jenis);


    @Query(value = "select sum(unit_ts_dua) as unitTsDua, sum(unit_ts_satu) as unitTsSatu, coalesce(sum(unit_ts), 0) as unitTs, sum(unit_ts_rata) as unitTsRata,\n" +
            "\tsum(akreditasi_ts_dua) as akreditasiTsDua, sum(akreditasi_ts_satu) as akreditasiTsSatu, coalesce(sum(akreditasi_ts), 0) as akreditasiTs, sum(akreditasi_ts_rata) as akreditasiTsRata\n" +
            "\tfrom keuangan where jenis in(?1, ?2)", nativeQuery = true)
    JumlahKeuanganDto sumBiayaPkm(String jenisSatu, String jenisKedua);

    @Query(value = "select sum(unit_ts_dua) as unitTsDua, sum(unit_ts_satu) as unitTsSatu, coalesce(sum(unit_ts), 0) as unitTs, sum(unit_ts_rata) as unitTsRata,\n" +
            "\tsum(akreditasi_ts_dua) as akreditasiTsDua, sum(akreditasi_ts_satu) as akreditasiTsSatu, coalesce(sum(akreditasi_ts), 0) as akreditasiTs, sum(akreditasi_ts_rata) as akreditasiTsRata\n" +
            "\tfrom keuangan where jenis in(?1, ?2, ?3)", nativeQuery = true)
    JumlahKeuanganDto sumBiayaInvestasi(String jenisSatu, String jenisKedua, String jenisKetiga);
}
