package id.ac.tazkia.spm.dao.config;


import id.ac.tazkia.spm.entity.config.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, String>, CrudRepository<Role, String> {


}
