package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.dto.AkreditasiExpiredDto;
import id.ac.tazkia.spm.entity.Institution;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface InstitutionDao extends PagingAndSortingRepository<Institution, String>, CrudRepository<Institution, String> {
    List<Institution> findByStatus(StatusRecord statusRecord);

    @Query(value = "select id, jenis, nama, kadaluarsa as tanggal, if(keterangan <= 0,'EXPIRED',if(keterangan = 1,concat(keterangan,' DAY TO EXPIRED'),concat(keterangan,' DAYS TO EXPIRED'))) as keterangan from\n" +
            "(select id,'INSTITUTION' as jenis, nama, coalesce(tanggal_kedaluwarsa,date(now())) as kadaluarsa,\n" +
            "datediff(coalesce(tanggal_kedaluwarsa,date(now())), date(now())) as keterangan  from institution where status = 'AKTIF'\n" +
            "and date_sub(coalesce(tanggal_kedaluwarsa,date(now())), interval 365 day) <= date(now())\n" +
            "union\n" +
            "select id,'PRODI' as jenis, nama_prodi as nama, coalesce(tgl_kadaluwarsa,date(now())) as kadaluarsa,\n" +
            "datediff(coalesce(tgl_kadaluwarsa,date(now())), date(now())) as keterangan  from prodi where status = 'AKTIF'\n" +
            "and date_sub(coalesce(tgl_kadaluwarsa,date(now())), interval 365 day) <= date(now())) as a\n" +
            "order by kadaluarsa asc", nativeQuery = true)
    List<AkreditasiExpiredDto> cariBatasAkreditasi();

}
