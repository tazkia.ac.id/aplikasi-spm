package id.ac.tazkia.spm.dao;

import id.ac.tazkia.spm.entity.Matakuliah;
import id.ac.tazkia.spm.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MatakuliahDao extends PagingAndSortingRepository<Matakuliah, String> {
    List<Matakuliah> findByStatus(StatusRecord statusRecord);
}
