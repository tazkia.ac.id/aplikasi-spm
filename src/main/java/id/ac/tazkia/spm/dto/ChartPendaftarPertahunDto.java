package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChartPendaftarPertahunDto {

    private List<String> daftarNama = new ArrayList<>();
    private List<Integer> daftarJumlah = new ArrayList<>();

}
