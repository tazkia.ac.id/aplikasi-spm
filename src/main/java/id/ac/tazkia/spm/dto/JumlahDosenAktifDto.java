package id.ac.tazkia.spm.dto;


import lombok.Data;

@Data
public class JumlahDosenAktifDto {

    private String kodeProdi;
    private Integer lb;
    private Integer hb;
    private Integer total;

}
