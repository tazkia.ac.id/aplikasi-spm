package id.ac.tazkia.spm.dto;

import lombok.Data;

@Data
public class JumlahPendaftarPertahunDto {

    private String nama;

    private Integer jumlah;


}
