package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class ChartTransaksiSetahunDto {

    private List<String> daftarBulan = new ArrayList<>();
    private List<BigDecimal> daftarTotal = new ArrayList<>();

}
