package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.math.BigDecimal;
public interface JumlahKeuanganDto {
    BigDecimal getUnitTsDua();
    BigDecimal getUnitTsSatu();
    BigDecimal getUnitTs();
    BigDecimal getUnitTsRata();
    BigDecimal getAkreditasiTsDua();
    BigDecimal getAkreditasiTsSatu();
    BigDecimal getAkreditasiTs();
    BigDecimal getAkreditasiTsRata();
}
