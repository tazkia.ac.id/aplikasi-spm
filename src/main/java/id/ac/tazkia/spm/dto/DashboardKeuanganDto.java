package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DashboardKeuanganDto {

    private String namaPeriodeAnggaran;
    private BigDecimal totalBudget;
    private BigDecimal requestFund;
    private BigDecimal totalPenerimaan;
    private BigDecimal budgetBalance;

}
