package id.ac.tazkia.spm.dto;

import lombok.Data;

@Data
public class JumlahPendaftarProdiDto {

    private String nama;

    private Integer jumlah;

}
