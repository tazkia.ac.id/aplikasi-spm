package id.ac.tazkia.spm.dto;

import java.time.LocalDate;

public interface AkreditasiExpiredDto {

    String getId();
    String getJenis();
    String getNama();
    LocalDate getTanggal();
    String getKeterangan();


}
