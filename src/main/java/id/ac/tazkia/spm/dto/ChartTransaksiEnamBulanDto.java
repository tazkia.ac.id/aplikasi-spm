package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class ChartTransaksiEnamBulanDto {

    private List<String> daftarNama = new ArrayList<>();
    private List<BigDecimal> daftarPemasukan = new ArrayList<>();
    private List<BigDecimal> daftarPengeluaran = new ArrayList<>();

}
