package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransaksiSetahunDuaDto {
    private BigDecimal total;
    private String bulan;
}
