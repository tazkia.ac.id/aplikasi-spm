package id.ac.tazkia.spm.dto;

import jakarta.persistence.Id;
import lombok.Data;

public interface KaryaIlmiahDto {
    String getId();
    String getMahasiswa();
    String getJudul();
    String getJumlahSitasi();
    String getStandar();
}
