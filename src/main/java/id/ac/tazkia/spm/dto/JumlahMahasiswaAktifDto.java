package id.ac.tazkia.spm.dto;

import lombok.Data;

@Data
public class JumlahMahasiswaAktifDto {

    private String kodeProdi;

    private Integer pria;

    private Integer wanita;

    private Integer total;

}
