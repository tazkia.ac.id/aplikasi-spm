package id.ac.tazkia.spm.dto;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChartPendaftarDto {

    private List<String> daftarProdi = new ArrayList<>();
    private List<Integer> daftarJumlah = new ArrayList<>();
    private List<String> daftarWarna = new ArrayList<>();

}
