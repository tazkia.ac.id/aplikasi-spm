package id.ac.tazkia.spm.dto;

public interface JumlahEmployeeDto {

    String getCompanyId();

    String getCompanyName();

    Integer getTotal();

    Integer getMale();

    Integer getFemale();

    Integer getPermanent();

    Integer getTemporary();

}
