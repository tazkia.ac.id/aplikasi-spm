package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChartDosenAktifDto {

    private List<String> daftarKodeProdi = new ArrayList<>();
    private List<Integer> daftarLb = new ArrayList<>();
    private List<Integer> daftarHb = new ArrayList<>();
    private List<Integer> daftarTotal = new ArrayList<>();

}
