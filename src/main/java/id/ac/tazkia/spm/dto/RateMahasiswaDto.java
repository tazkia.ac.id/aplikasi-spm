package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RateMahasiswaDto {

    private String bulan;
    private BigDecimal rate;

}
