package id.ac.tazkia.spm.dto;

import jakarta.persistence.Id;
import lombok.Data;


@Data
public class JumlahEmployeeDuaDto {

    @Id
    private String companyId;

    private String companyName;

    private Integer total;

    private Integer male;

    private Integer female;

    private Integer permanent;

    private Integer temporary;
}
