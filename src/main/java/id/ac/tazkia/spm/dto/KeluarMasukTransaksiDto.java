package id.ac.tazkia.spm.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class KeluarMasukTransaksiDto {

    private Integer ada;
    private String nama;
    private BigDecimal penerimaan;
    private BigDecimal pengeluaran;

}
