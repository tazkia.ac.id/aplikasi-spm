package id.ac.tazkia.spm.dto;


import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class ChartMahasiswaAktifDto {

    private List<String> daftarKodeProdi = new ArrayList<>();
    private List<Integer> daftarMale = new ArrayList<>();
    private List<Integer> daftarFemale = new ArrayList<>();
    private List<Integer> daftarTotal = new ArrayList<>();

}
