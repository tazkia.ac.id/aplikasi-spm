package id.ac.tazkia.spm.dto;

import java.math.BigDecimal;

public interface TransaksiSetahunDto {

    BigDecimal getTotal();

    String getBulan();

}
