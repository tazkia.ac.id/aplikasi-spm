CREATE TABLE `finance_dashboard` (
                                     `id` varchar(45) COLLATE utf8mb3_unicode_ci NOT NULL,
                                     `nama_periode_anggaran` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                     `total_budget` decimal(19,2) DEFAULT NULL,
                                     `request_fund` decimal(19,2) DEFAULT NULL,
                                     `total_penerimaan` decimal(19,2) DEFAULT NULL,
                                     `budget_balance` decimal(19,2) DEFAULT NULL,
                                     `status` varchar(15) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `finance_transaksi_enam_bulan` (
                                                `id` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL,
                                                `ada` int DEFAULT NULL,
                                                `nama` varchar(100) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                                `penerimaan` decimal(19,2) DEFAULT NULL,
                                                `pengeluaran` decimal(19,2) DEFAULT NULL,
                                                `status` varchar(10) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `smile_jumlah_dosen_aktif` (
                                            `id` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL,
                                            `kode_prodi` varchar(45) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                            `lb` int DEFAULT NULL,
                                            `hb` int DEFAULT NULL,
                                            `total` int DEFAULT NULL,
                                            `status` varchar(10) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `smile_jumlah_mahasiswa_aktif` (
                                                `id` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL,
                                                `kode_prodi` varchar(45) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                                `pria` decimal(19,2) DEFAULT NULL,
                                                `wanita` decimal(19,2) DEFAULT NULL,
                                                `total` decimal(19,2) DEFAULT NULL,
                                                `status` varchar(10) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `smile_performance_mahasiswa` (
                                               `id` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL,
                                               `tahun` varchar(4) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                               `bulan` varchar(45) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                               `rate` decimal(10,2) DEFAULT NULL,
                                               `status` varchar(10) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `spmb_pendaftar_per_prodi` (
                                            `id` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                                            `nama` varchar(100) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                            `jumlah` int DEFAULT NULL,
                                            `status` varchar(8) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `spmb_pendaftar_per_tahun` (
                                            `id` varchar(40) COLLATE utf8mb3_unicode_ci NOT NULL,
                                            `nomor` int DEFAULT NULL,
                                            `nama` varchar(105) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                            `jumlah` int DEFAULT NULL,
                                            `status` varchar(8) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
